/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.sp.outputoperator.outputoperator.file;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author loliveira
 */
public class FileHandlerThread implements Runnable {

    private String fileName;
    private String type;
    private String text;

    public FileHandlerThread(String fileName, String type, String text) {
        this.fileName = fileName;
        this.type = type;
        this.text = text;
    }

    @Override
    public void run() {
        write();
    }

    private void write() {
        try {
            if (!type.isEmpty()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

                String today = sdf.format(new Date());

                File dateDir = new File(today);
                if (!dateDir.exists()) {
                    dateDir.mkdir();
                }

                File typeDir = new File(today + File.separator + type);

                if (!typeDir.exists()) {
                    typeDir.mkdir();
                }

                FileWriter fw;
                File f = new File(typeDir.getPath(), fileName);
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                }
                fw = new FileWriter(f.getPath(), true);

                fw.write(text);
                fw.close();
            } else {
                FileWriter fw;
                File f = new File(fileName);
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                }
                fw = new FileWriter(f.getPath(), true);

                fw.write(text);
                fw.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
