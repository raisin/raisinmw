/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.sp.outputoperator.outputoperator.file;

import java.util.HashMap;

/**
 *
 * @author loliveira
 */
public class MetaDataFile {

    private String ipMIA ;

    private String ipMIAOPE = "smb://192.168.1.36//C$//DocOrigin3.0//User//FolderMonitor//PRD//Jobs//";

    private String userMIA = "JFUSER";

    private String passwordMia = "";

    private String ipSWIFT = "10.0.1.34@/media/databases/SIBS/MFT/env/bai/saa/datain/";
    
    private String ipSWIFTPassTrough = "10.0.1.34@/media/databases/SIBS/MFT/env/bai/saa/datasp/";

    private String userSWIFT = "";

    private String passwordSWIFT = "";

    public MetaDataFile(String ip, String version) {
        HashMap<String, String> hash = FileHandler.readFileAsHash("file"+ip.replaceAll("\\.", "") + version);
        
        ipMIA = hash.get("ipMIA");
        ipMIAOPE =hash.get("ipMIAOPE");
        userMIA = hash.get("userMIA");
        passwordMia = hash.get("passwordMia");
        ipSWIFT = hash.get("ipSWIFT");
        userSWIFT = hash.get("userSWIFT");
        passwordSWIFT= hash.get("passwordSWIFT");
        ipSWIFTPassTrough = hash.get("ipSWIFTPassTrough");
    }

    public String getIpMIA() {
        return ipMIA;
    }

    public void setIpMIA(String ipMIA) {
        System.out.println(ipMIA);
        this.ipMIA = ipMIA;
    }

    public String getUserMIA() {
        return userMIA;
    }

    public void setUserMIA(String userMIA) {
        System.out.println(userMIA);
        this.userMIA = userMIA;
    }

    public String getPasswordMia() {
        return passwordMia;
    }

    public void setPasswordMia(String passwordMia) {
        System.out.println(passwordMia);
        this.passwordMia = passwordMia;
    }

    public String getIpSWIFT() {
        return ipSWIFT;
    }

    public void setIpSWIFT(String ipSWIFT) {
        System.out.println(ipSWIFT);
        this.ipSWIFT = ipSWIFT;
    }

    public String getUserSWIFT() {
        return userSWIFT;
    }

    public void setUserSWIFT(String userSWIFT) {
        System.out.println(userSWIFT);
        this.userSWIFT = userSWIFT;
    }

    public String getPasswordSWIFT() {
        return passwordSWIFT;
    }

    public void setPasswordSWIFT(String passwordSWIFT) {
        System.out.println(passwordSWIFT);
        this.passwordSWIFT = passwordSWIFT;
    }

    public String getIpMIAOPE() {
        return ipMIAOPE;
    }

    public void setIpMIAOPE(String ipMIAOPE) {
        System.out.println(ipMIAOPE);
        this.ipMIAOPE = ipMIAOPE;
    }

    public String getIpSWIFTPassTrough() {
        return ipSWIFTPassTrough;
    }

    public void setIpSWIFTPassTrough(String ipSWIFTPassTrough) {
        this.ipSWIFTPassTrough = ipSWIFTPassTrough;
    }
}
