/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.sp.outputoperator.outputoperator.file;

import com.jcraft.jsch.*;
import jcifs.smb.*;
import org.apache.log4j.Logger;
import pt.bai.sp.outputoperator.mail.MetaDataMail;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author loliveira
 */
public class FileHandler {

    private static Logger log = Logger.getLogger(FileHandler.class.getName());

    public static void WriteToFile(String fileName, String type, String text) {
        Thread t = new Thread(new FileHandlerThread(fileName, type, text));
        t.start();
    }

    public static void WriteToRemoteFile(MetaDataFile metaFile, MetaDataMail metaMail, String fileName,
            String extension, String text, String remotelocation, String user, String password, String domain) {

        if (remotelocation.toUpperCase().contains("SMB")) {

            String fileRemoteName = "EMPTY";

            try {
                FileWriter fw;
                java.io.File f = new java.io.File(fileName);
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                }
                fw = new FileWriter(fileName, true);

                fw.write(text);
                fw.close();

                // Date d = new Date();
                fileRemoteName = remotelocation + fileName;

                SmbFile newFile = new SmbFile(fileRemoteName + ".tmp",
                        new NtlmPasswordAuthentication(domain, user, password));

                newFile.createNewFile();

                SmbFileOutputStream sfos = new SmbFileOutputStream(newFile);
                sfos.write(text.getBytes("CP1252"));
                sfos.close();

                newFile.renameTo(new SmbFile(fileRemoteName + "." + extension,
                        new NtlmPasswordAuthentication(domain, user, password)));

                if (f.exists()) {
                    f.delete();
                }
                ;

            } catch (IOException ex) {
                log.error(ex.getMessage());
            }
        } else {

            try {
                String[] split = remotelocation.split("\\@");

                Session session = createSession(user, split[0], 22, password);

                // System.out.println("ENTRAR EM " + user + " " + split[0] + " " + password);
                log.debug("ENTRAR EM " + user + " " + split[0]);

                FileWriter fw;
                java.io.File f = new java.io.File(fileName);
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                }
                fw = new FileWriter(fileName, true);

                fw.write(text);
                fw.close();

                copyLocalToRemote(session, split[1], fileName, f);

                if (f.exists()) {
                    f.delete();
                }
                ;
            } catch (JSchException | IOException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    public static void writeToRemoteFile(String fileName, String extension, String text, String remoteTempLocation,
            String user, String password, String domain, String finalLocation) {

        if (remoteTempLocation.toUpperCase().contains("SMB")) {

            String fileRemoteName = "EMPTY";
            log.debug("ENTRAR EM " + user + " " + remoteTempLocation);
            try {
                FileWriter fw;
                java.io.File f = new java.io.File(fileName);
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                }
                fw = new FileWriter(fileName, true);

                fw.write(text);
                fw.close();

                // Date d = new Date();
                fileRemoteName = remoteTempLocation + fileName;

                SmbFile newFile = new SmbFile(fileRemoteName + ".tmp",
                        new NtlmPasswordAuthentication(domain, user, password));

                newFile.createNewFile();

                SmbFileOutputStream sfos = new SmbFileOutputStream(newFile);
                sfos.write(text.getBytes("CP1252"));
                sfos.close();

                newFile.renameTo(new SmbFile(fileRemoteName + "." + extension,
                        new NtlmPasswordAuthentication(domain, user, password)));

                if (f.exists()) {
                    f.delete();
                }
                log.debug("All done....");

            } catch (IOException ex) {
                log.error(ex.getMessage());
            }
        } else {

            try {
                String[] split = remoteTempLocation.split("\\@");

                Session session = createSession(user, split[0], 22, password);

                log.debug("ENTRAR EM " + user + " " + split[0]);

                FileWriter fw;
                java.io.File f = new java.io.File(fileName);
                if (f.exists()) {
                    f.delete();
                    f.createNewFile();
                }
                fw = new FileWriter(fileName, true);

                fw.write(text);
                fw.close();

                copyLocalToRemote(session, split[1], fileName, f);

                session = createSession(user, split[0], 22, password);
                moveFileRemote(session, fileName, split[1], finalLocation);
                if (f.exists()) {
                    f.delete();
                }

                log.debug("All done!");
            } catch (JSchException | IOException ex) {
                log.error(ex.getMessage());
            }
        }
    }

    public static List<String> readFileAsList(String file) {
        Path wiki_path = Paths.get("", file);

        try {
            List<String> lines = Files.readAllLines(wiki_path, Charset.defaultCharset());

            return lines;
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    public static HashMap<String, String> readFileAsHash(String file) {
        Path wiki_path = Paths.get("", file);

        try {
            List<String> lines = Files.readAllLines(wiki_path, Charset.defaultCharset());

            HashMap<String, String> hash = new HashMap<>();

            for (String line : lines) {
                String[] split = line.split("\\|\\|");
                hash.put(split[0], split[1]);
            }
            return hash;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return null;
        }
    }

    public static SmbFile lastFileModified(String remotelocation, String dir, String user, String password,
            String domain) {
        try {
            SmbFile fl = new SmbFile(remotelocation + dir, new NtlmPasswordAuthentication(domain, user, password));

            SmbFile[] files = fl.listFiles(new SmbFileFilter() {
                public boolean accept(SmbFile file) {
                    try {
                        return file.isFile();
                    } catch (SmbException ex) {
                        return false;
                    }
                }
            });
            long lastMod = Long.MIN_VALUE;
            SmbFile choice = null;
            for (SmbFile file : files) {
                if (file.lastModified() > lastMod) {
                    choice = file;
                    lastMod = file.lastModified();
                }
            }

            return choice;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return null;
        }
    }

    public static void getRMA() {
        try {
            SmbFile smb = lastFileModified("smb://192.168.1.33//D$//Alliance//SERVER//report//", "", "",
                    "", "");

            String toWrite = "";

            BufferedReader br = new BufferedReader(new InputStreamReader(smb.getInputStream()));

            String line;

            while ((line = br.readLine()) != null) {
                String[] lineSplit = line.split(" ");
                if (lineSplit[lineSplit.length - 1].equals("Send")) {
                    toWrite = toWrite + lineSplit[0] + "\n";
                }
            }

            // WriteToFile("list.lo", "", toWrite);
        } catch (IOException ex) {
            log.error(ex.getMessage());
        }
    }

    private static Session createSession(String user, String host, int port, String keyPassword) {
        try {
            JSch jsch = new JSch();
            log.debug("Criar sessao");

            Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");

            Session session = jsch.getSession(user, host, port);
            session.setConfig(config);
            session.setPassword(keyPassword);
            session.connect();

            return session;
        } catch (JSchException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    private static void copyLocalToRemote(Session session, String to, String fileName, File file)
            throws JSchException, IOException {
        boolean ptimestamp = true;
        String from = fileName;

        String command = "scp " + (ptimestamp ? "-p" : "") + " -t " + to;
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);

        OutputStream out = channel.getOutputStream();
        InputStream in = channel.getInputStream();

        channel.connect();

        if (checkAck(in) != 0) {
            System.exit(0);
        }

        if (ptimestamp) {
            command = "T" + (file.lastModified() / 1000) + " 0";
            command += (" " + (file.lastModified() / 1000) + " 0\n");
            out.write(command.getBytes());
            out.flush();
            if (checkAck(in) != 0) {
                System.exit(0);
            }
        }

        long filesize = file.length();
        command = "C0644 " + filesize + " ";
        if (from.lastIndexOf('/') > 0) {
            command += from.substring(from.lastIndexOf('/') + 1);
        } else {
            command += from;
        }

        command += "\n";
        out.write(command.getBytes());
        out.flush();

        if (checkAck(in) != 0) {
            System.exit(0);
        }

        FileInputStream fis = new FileInputStream(from);
        byte[] buf = new byte[1024];
        while (true) {
            int len = fis.read(buf, 0, buf.length);
            if (len <= 0) {
                break;
            }
            out.write(buf, 0, len); // out.flush();
        }

        buf[0] = 0;
        out.write(buf, 0, 1);
        out.flush();

        if (checkAck(in) != 0) {
            System.exit(0);
        }
        out.close();

        try {
            if (fis != null) {
                fis.close();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }

        channel.disconnect();
        session.disconnect();
    }

    private static void moveFileRemote(Session session, String fileName, String tempPath, String finalPath)
            throws JSchException, IOException {

        String command = "cd " + tempPath + " && mv " + fileName + " " + finalPath;
        log.debug(command);
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);

        channel.connect();

        OutputStream out = channel.getOutputStream();

        out.write(command.getBytes());

        out.close();

        channel.disconnect();
        session.disconnect();
    }

    public static int checkAck(InputStream in) throws IOException {
        int b = in.read();

        if (b == 0) {
            return b;
        }
        if (b == -1) {
            return b;
        }

        if (b == 1 || b == 2) {
            StringBuffer sb = new StringBuffer();
            int c;
            do {
                c = in.read();
                sb.append((char) c);
            } while (c != '\n');
            if (b == 1) { // error
                log.debug(sb.toString());
            }
            if (b == 2) { // fatal error
                log.debug(sb.toString());
            }
        }
        return b;
    }

}
