/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.sp.outputoperator.outputoperator.mail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 *
 * @author loliveira
 */
public class Mailserver {

    public static void sendMailErrorSWIFT(MetaDataMail meta,String reference, String body) throws UnsupportedEncodingException, NoSuchProviderException {
        String from = meta.getMailFrom();
        String host = meta.getMailHost();
        final String user = meta.getMailFrom();
        final String password = meta.getMailPassword();

        Properties props = new Properties();

        props.put("mail.smtp.host", host);
        props.put("mail.debug", meta.getMailDebug());
        props.put("mail.smtp.auth", meta.getMailAuth());
        props.put("mail.smtp.starttls.enable", meta.getMailTLS());
        props.put("mail.smtp.port", meta.getMailPort());
        

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        });

        session.getTransport("smtp");

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from, "Apoio"));

            String[] split = null;

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress("l.oliveira@bailisboa.pt", "Luis Oliveira"));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress("dop@bailisboa.pt", "DOP"));

            msg.setSubject("Sistema de Pagamentos");

            MimeBodyPart p1 = new MimeBodyPart();
            p1.setText("O envio das Mensagens SWIFT para operacao " + reference + " nao foi bem sucedido.\n"+body);

            // Create second part
            // Create the Multipart.  Add BodyParts to it.
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(p1);

            // Set Multipart as the message's content
            msg.setContent(mp);

            //Send the message
            Transport.send(msg);

        } catch (MessagingException mex) {
            // Prints all nested (chained) exceptions as well
            mex.printStackTrace();
        }
    }

    public static void sendMailFreeText(MetaDataMail meta,String text, String name, String mail) throws UnsupportedEncodingException, NoSuchProviderException {
        String from = meta.getMailFrom();
        String host = meta.getMailHost();
        final String user = meta.getMailFrom();
        final String password = meta.getMailPassword();

        Properties props = new Properties();

        props.put("mail.smtp.host", host);
        props.put("mail.debug", meta.getMailDebug());
        props.put("mail.smtp.auth", meta.getMailAuth());
        props.put("mail.smtp.starttls.enable", meta.getMailTLS());
        props.put("mail.smtp.port", meta.getMailPort());

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, password);
            }
        });

        session.getTransport("smtps");

        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from, "Apoio"));

            String[] split = null;

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(mail, name));

            msg.setSubject("Sistema de Pagamentos");

            MimeBodyPart p1 = new MimeBodyPart();
            p1.setText(text);

            // Create second part
            // Create the Multipart.  Add BodyParts to it.
            Multipart mp = new MimeMultipart();
            mp.addBodyPart(p1);

            // Set Multipart as the message's content
            msg.setContent(mp);

            //Send the message
            Transport.send(msg);

        } catch (MessagingException mex) {
            // Prints all nested (chained) exceptions as well
            mex.printStackTrace();
        }
    }

    public static void main(String[] args) {
//        try {
//            
//            MetaDataMail meta = new MetaDataMail();
//            Mailserver.sendMailErrorSWIFT(meta,"AAAA","");
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(Mailserver.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NoSuchProviderException ex) {
//            Logger.getLogger(Mailserver.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

}
