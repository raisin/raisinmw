/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.sp.outputoperator.outputoperator.mail;

import pt.bai.sp.outputoperator.file.FileHandler;

import java.util.HashMap;

/**
 *
 * @author loliveira
 */
public class MetaDataMail {
    
    private String mailFrom;
    private String mailHost;

    private String mailPassword;
    private String mailDebug;
    private String mailAuth;
    private String mailTLS;
    private String mailPort;
    
    private String mailDop;

    public MetaDataMail(String ip, String version) {
        HashMap<String, String> hash = FileHandler.readFileAsHash("mail"+ip.replaceAll("\\.", "") + version);
        
        mailFrom = hash.get("mailFrom");
        mailHost =hash.get("mailHost");
        mailPassword = hash.get("mailPassword");
        mailDebug = hash.get("mailDebug");
        mailAuth = hash.get("mailAuth");
        mailTLS = hash.get("mailTLS");
        mailPort= hash.get("mailPort");
        mailDop= hash.get("mailDop");
    }
    
    public String getMailAuth() {
        return mailAuth;
    }

    public String getMailDebug() {
        return mailDebug;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public String getMailHost() {
        return mailHost;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public String getMailPort() {
        return mailPort;
    }

    public String getMailTLS() {
        return mailTLS;
    }

    public String getMailDop() {
        return mailDop;
    }
    
}
