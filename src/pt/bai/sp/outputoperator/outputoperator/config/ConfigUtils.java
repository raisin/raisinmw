package pt.bai.sp.outputoperator.outputoperator.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConfigUtils {

	private Properties config;

	public Properties loadProps(String fileName) {
		config = new Properties();

		try {
			config.load(new FileInputStream(fileName));
		} catch (IOException e) {
			Logger.getLogger(ConfigUtils.class.getName()).log(Level.SEVERE, null, e);
		}
		return config;
	}

}
