/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.sp.outputoperator.outputoperator.output;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author loliveira
 */
public class OutputWriter implements Runnable {
	
	private static Logger log = Logger.getLogger(OutputWriter.class.getName());

    private String toWrite;

    public OutputWriter(String toWrite) {
        this.toWrite = toWrite;
    }

    @Override
    public void run() {
        write();
    }

    private void write() {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        synchronized (System.out) {
        	log.info(sdf.format(d) + " - " + toWrite);
        }
    }

}
