/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.sp.outputoperator.outputoperator.output;

/**
 *
 * @author loliveira
 */
public class OutputWriterStarter {

    public static void write(String toWrite, boolean output) {
        if (output) {
            Thread t = new Thread(new OutputWriter(toWrite));
            t.start(); t.interrupt();
        }
    }

}
