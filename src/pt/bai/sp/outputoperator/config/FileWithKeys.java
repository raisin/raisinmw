package pt.bai.sp.outputoperator.config;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class allows to read file with bic code and check if the list contains a given bic code.
 * @author rraposo
 *
 */
public class FileWithKeys {
	
	private List<String> bicList;
	
	public FileWithKeys(String pathLocation) {
		readFile(pathLocation);
	}
	
	/**
	 * This method read the properties file "list.lo", that contains all bic code which we have keys to communication.
	 * The result of this method is the instantiation of bicList parameter.
	 */
	private void readFile(String pathLocation) {
		BufferedReader reader = null;

		try {
//			File jarPath=new File(ConfigUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath());
//	        String propertiesPath=jarPath.getParentFile().getAbsolutePath()+"/../list.lo";
	        
	        FileReader fr = new FileReader(pathLocation);
	        reader = new BufferedReader(fr);
			
//			InputStream in = FileWithKeys.class.getClassLoader().getResourceAsStream("list.lo");
//			reader = new BufferedReader(new InputStreamReader(in));
			bicList = new ArrayList<>();
			String line;
			while ((line = reader.readLine()) != null) {
				bicList.add(line);
			}

		} catch (IOException e) {
			Logger.getLogger(FileWithKeys.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					Logger.getLogger(FileWithKeys.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
	}
	
	/**
	 * Validate if the bic is inside the list of bic's with keys to communication.
	 * @param bic
	 * @return true if we have keys with bic, and false otherwise.
	 */
	public boolean hasKeysWithBic(String bic) {
		return bicList.contains(bic);
	}
	
}
