package pt.bai.sp.outputoperator.wrapper;

import java.util.Objects;

public class Pair<K, V> {

    private final K key;
    public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	private final V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public boolean equals(Object o) {
        return o instanceof Pair && Objects.equals(key, ((Pair<?,?>)o).key) && Objects.equals(value, ((Pair<?,?>)o).value);
    }

    public int hashCode() {
        return 31 * Objects.hashCode(key) + Objects.hashCode(value);
    }

    public String toString() {
        return key + "=" + value;
    }
}
