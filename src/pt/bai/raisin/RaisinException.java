package pt.bai.raisin;

public class RaisinException extends Exception {

	public RaisinException(String message) {

		super(message.replaceAll("\\n", "->"));
	}

}
