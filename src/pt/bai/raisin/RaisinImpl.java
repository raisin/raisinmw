package pt.bai.raisin;

import com.mongodb.client.model.Filters;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import pt.bai.dbmanager.Manager;
import pt.bai.dbmanager.wrappers.ClientAccount;
import pt.bai.dbmanager.wrappers.PairWithGenerics;
import pt.bai.dbmanager.wrappers.ResultContacliente;
import pt.bai.raisin.db.banka.BankaConnection;
import pt.bai.raisin.db.mongo.MongoConnection;
import pt.bai.raisin.db.postgres.HibernateUtils;
import pt.bai.raisin.db.postgres.entities.workflow.*;

import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

/**
 * Class that implements logic of Raisin project
 * 
 * @author rraposo
 *
 */
public class RaisinImpl {

	private final static Logger LOGGER = Logger.getLogger(RaisinImpl.class);

	private HibernateUtils hibernateUtils;
	private MongoConnection mongo;

	public RaisinImpl() {

		hibernateUtils = HibernateUtils.getInstance();
		mongo = MongoConnection.getInstance();
	}

	/**
	 * Gives the following Edge from the given requestId and function.
	 * 
	 * @param function string with function to evolute
	 * @param hibernateUtils
	 * @param reqId integer id of request
	 * @return the next Edge
	 */
	public Edge getNextEdge(String function, HibernateUtils hibernateUtils, int reqId) {
		try {
			LOGGER.info("Using " + function + " function for request id " + reqId);
			Query query = hibernateUtils.getEntityManager().createQuery(
					"SELECT p FROM Position p WHERE p.request.id = :requestId AND p.workflow.id = :workflowId ");
			query.setParameter("requestId", reqId);
			query.setParameter("workflowId", 3);

			Position position = (Position) (query.getResultList().size() == 0 ? null : query.getResultList().get(0));

			Query queryEdges = hibernateUtils.getEntityManager().createQuery(
					"SELECT e FROM Edge e WHERE e.stageByStageFrom.id = :stageId AND e.function = :function ");
			queryEdges.setParameter("stageId", position.getStage().getId());
			queryEdges.setParameter("function", function);

			Edge edge = (Edge) (queryEdges.getResultList().size() == 0 ? null : queryEdges.getResultList().get(0));
			LOGGER.info("Moving file to stage " + edge.getStageByStageTo().getId());
			return edge;
		} catch (Exception e) {
			LOGGER.error("Request without a valid stage", e);
			throw e;
		}
	}

	/**
	 * This function is responsible for change the Position and adds History of the
	 * evolution.
	 * 
	 * @param request integer id of referred Request
	 * @param stageTo  
	 * @param userId user that evolute the request
	 */
	public void evoluteRequest(int requestId, Stage stageTo, int userId) {

		try {
			LOGGER.info( "Moving the request id " + requestId + " to stage " + stageTo.getId() + " by user " + userId);
			Workflow workflow = hibernateUtils.getEntityManager().find(Workflow.class, 3);
			hibernateUtils.getEntityManager().getTransaction().begin();
			Query queryPosition = hibernateUtils.getEntityManager().createQuery(
					"UPDATE Position SET change_date = :date, stage_id = :stageId where request_id = :requestId and workflow_id = :workflowId ");
			queryPosition.setParameter("date", new Date());
			queryPosition.setParameter("stageId", stageTo.getId());
			queryPosition.setParameter("requestId", requestId);
			queryPosition.setParameter("workflowId", workflow.getId());
			queryPosition.executeUpdate();
			LOGGER.info("Request id " + requestId + " updated successfully.");

			hibernateUtils.getEntityManager().flush();
			Request request = hibernateUtils.getEntityManager().find(Request.class, requestId);

			Stage stage = hibernateUtils.getEntityManager().find(Stage.class, stageTo.getId());
			History history = new History(
					new HistoryId(request.getId(), stageTo.getId(), workflow.getId(), userId, "N/A", new Date()),
					request, stage, workflow);
			LOGGER.info("History table updated.");

			hibernateUtils.getEntityManager().persist(history);
			hibernateUtils.getEntityManager().flush();
			hibernateUtils.getEntityManager().getTransaction().commit();
		} catch (Exception e) {
			hibernateUtils.getEntityManager().getTransaction().rollback();
			LOGGER.error("Failed to move to next stage: ", e);
			throw e;
		}
	}

	/**
	 * This method search for entity by parameter name.
	 * 
	 * @param name a string of name of Entity.
	 * @return true if exists an entity with parameter name.
	 * @throws RaisinException in case of an error occurred, like Banka is in "Fecho de dia".
	 */
	private boolean findEntidadeByName(String name) throws RaisinException {
		LOGGER.info("Trying to find Entity " + name);
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {

			boolean entityExists = false;
			if (managerDB.getEntityByName(name) != null) {
				entityExists = true;
				LOGGER.warn("Entity Found: " + name);
			}
			managerDB.closeAll();
			return entityExists;
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}

	}

	/**
	 * This method search for entity by parameters docType and its number.
	 * 
	 * @param docType type of the Document Id of Banka (P:331)
	 * @param docNumber number of doc type
	 * @return true if exists an Entity by the type and number.
	 * @throws RaisinException throws an Exception if Banka system is down.
	 */
	private boolean findEntidadeByDocTypeAndNumber(int docType, String docNumber) throws RaisinException {
		LOGGER.info("Trying to find entity docType " + docType + " and docNumber " + docNumber);
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			boolean entityExists = managerDB.getEntityByDocTypeAndDocNumber(docType, docNumber);
			LOGGER.debug("Entity was found? " + entityExists);
			managerDB.closeAll();
			return entityExists;
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
	}

	/**
	 * This method creates an Entity on Banka system.
	 * 
	 * @param context string that represents the "_id" of MongoDb.
	 * @throws Exception
	 */
	public void createEntity(String context) throws Exception {
		LOGGER.info("Starting to creat an Entity");
		try {
			Document doc = mongo.get("_id", new ObjectId(context));

			boolean isEntityByName = findEntidadeByName(
					doc.getString("first_names").toUpperCase() + " " + doc.getString("last_name").toUpperCase());
			int idType;
			if ("Passport".equals(doc.get("id_type"))) { // validate if we should lookup for "Passport", alternative?
				idType = 302;
			} else {
				idType = 301;
			}
			boolean isEntityByDoc = findEntidadeByDocTypeAndNumber(idType, doc.getString("id_number"));

			if (!isEntityByName && !isEntityByDoc) {
				String numEntidade = callCreateEntityWS(doc);
				try {
					LOGGER.debug("Entity number " + numEntidade);
					int nEntidade = Integer.parseInt(numEntidade);
					mongo.updateDoc("_id", doc.getObjectId("_id"), "entityId", nEntidade);
					int requestId = doc.getInteger("request_id");
					Edge edge2 = getNextEdge("EntityCreation", hibernateUtils, requestId);
					evoluteRequest(requestId, edge2.getStageByStageTo(), 5);
				} catch (NumberFormatException e) {
					throw new RaisinException("Failed to creat the Entity " + numEntidade);
				}

			} else {
				LOGGER.info("Entity already created.");

				List<Bson> filterList = new ArrayList<>();
				filterList.add(Filters.eq("first_names", Pattern.compile(doc.getString("first_names"), Pattern.CASE_INSENSITIVE) ));
				filterList.add(Filters.eq("last_name",  Pattern.compile(doc.getString("last_name"), Pattern.CASE_INSENSITIVE) ));
				
				filterList.add(Filters.eq("tax_id", doc.getString("tax_id")));
				
				Document docFound = mongo.getWithAnd(filterList);
				LOGGER.debug("end of query... was found? " + (docFound != null));
				if (docFound != null) {
					mongo.updateDoc("_id", doc.getObjectId("_id"), "entityId", docFound.getInteger("entityId") );
					mongo.updateDoc("_id", doc.getObjectId("_id"), "clientId", docFound.getInteger("clientId") );
					mongo.updateDoc("_id", doc.getObjectId("_id"), "accountId", docFound.getLong("accountId") );
					LOGGER.debug("Process ID = " + doc.getObjectId("_id") + " updated entityNr = " + docFound.getInteger("entityId"));
				} else {
					LOGGER.warn("Entity NOT FOUND");
					throw new RaisinException("Entity NOT FOUND for taxId " + doc.getString("tax_id")
						+ " and name " + doc.getString("first_names") + " " + doc.getString("last_name"));
				}
			}

		} catch (Exception e) {
			System.err.println("Failed to create an entity: " + e);
			LOGGER.error("Failed to creat an entity: ", e);
			throw e;
		}
	}

	/**
	 * This method invokes a web service to create new Entity on Banka.
	 * 
	 * @param doc a MongoDb Document with structure of Entity Info.
	 * @return the Entity number or its error if something went wrong.
	 * @throws IOException
	 */
	private String callCreateEntityWS(Document doc) throws IOException {
		HttpURLConnection conn = null;
		try {
			BankaConnection banka = BankaConnection.getInstance();
			LOGGER.info("Banka connection opened.");


			URL url = new URL("http://10.0.2.33:8080/BankaWebservices/createEntity");

			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "multipart/form-data");

			LOGGER.info("Creating an entity by the Web Service - (POST) at the address: " + url);
			String cardIdCountry = "";
			cardIdCountry = doc.getString("address_country");

			String idType = null;
			if ("Passport".equals(doc.get("id_type"))) {
				idType = "302";
			} else {
				idType = "301";
			}
			String tipoEntidade = "D";

			Document docMaritalStatus = mongo.getValueFromAnotherCollection("MaritalStatus", "descriptionRaisin",
					doc.getString("marital_status"));

			conn.setRequestProperty("estacao", banka.getEstacao());
			conn.setRequestProperty("utilizador", banka.getUser());
			conn.setRequestProperty("nomeEntidade", validateNameSize(doc));
			conn.setRequestProperty("dataNascimento", doc.getString("birth_dt").replaceAll("-", ""));
			conn.setRequestProperty("naturalidade", normalize(doc.getString("place_of_birth")));
			conn.setRequestProperty("nacionalidade", doc.getString("nationality"));
			conn.setRequestProperty("estadoCivil", docMaritalStatus.getString("siglaBanka"));
			if ("C".equals(docMaritalStatus.getString("siglaBanka"))) {
				conn.setRequestProperty("regimeCasamento", "O");
			} else {
				conn.setRequestProperty("regimeCasamento", "");
			}
			conn.setRequestProperty("morada", normalize(doc.getString("address_street") + " " 
					+ doc.getString("address_postal_cd") + " " + doc.getString("address_town")));
			conn.setRequestProperty("tipoEntidade", tipoEntidade);
			conn.setRequestProperty("codigoResidencia", "E");
			conn.setRequestProperty("pais", doc.getString("address_country"));
			conn.setRequestProperty("codigoSectorial", "421000");
			conn.setRequestProperty("sexo", doc.getString("gender_cd").substring(0, 1));
			conn.setRequestProperty("habilitacoes", doc.getString("education"));
			conn.setRequestProperty("profissao", doc.getString("industry"));
			conn.setRequestProperty("entidadePatronal", normalize(doc.getString("employer")));
			conn.setRequestProperty("tipoDocumentoIdentificacao", idType);
			conn.setRequestProperty("numeroDocumentoIdentificacao", doc.getString("id_number"));
			conn.setRequestProperty("checkIdentificacao", "0");
			conn.setRequestProperty("referenciaEmissao", "ALEMANHA");
			conn.setRequestProperty("tipoDocumentoIdentificacao2", "510");
			conn.setRequestProperty("numeroDocumentoIdentificacao2",
					doc.getString("term_deposit_id").replaceAll("[^0-9]", ""));
			conn.setRequestProperty("checkIdentificacao2", "0");
			conn.setRequestProperty("referenciaEmissao2", "");
			conn.setRequestProperty("validadeDocumento2", "0");
			conn.setRequestProperty("actividadeProfissional", doc.getString("profession"));
			conn.setRequestProperty("validadeDocumento", doc.getString("id_expiry_dt").replaceAll("-", ""));
			conn.setRequestProperty("telefone", "");
			conn.setRequestProperty("nomePai", "");
			conn.setRequestProperty("nomeMae", "");
			conn.setRequestProperty("recebeEmail", "N");
			conn.setRequestProperty("recebeSMS", "N");
			conn.setRequestProperty("paisEmissaoDoc1", cardIdCountry);
			conn.setRequestProperty("paisEmissaoDoc2", "PRT");
			conn.setRequestProperty("aberturaPresencial", "N");
			conn.setRequestProperty("efacta", "Non U.S. Person");
			conn.setRequestProperty("dataRevisao", doc.getString("id_expiry_dt").replaceAll("-", ""));
			conn.setRequestProperty("classCRS", "CRS102 - Part. ou ENF Activa Rep.");
			conn.setRequestProperty("catBenCRS", URLEncoder.encode("Não é Beneficiário Efetivo", "UTF-8"));
			conn.setRequestProperty("nife", doc.getString("tax_id"));
			conn.setRequestProperty("paisnife", doc.getString("tax_country"));

			if (doc.containsKey("nationality_secondary") && doc.getString("nationality_secondary").length() == 3) {
				conn.setRequestProperty("nacio2", doc.getString("nationality_secondary"));
				conn.setRequestProperty("nacio3", "");
			} else {
				conn.setRequestProperty("nacio2", "");
				conn.setRequestProperty("nacio3", "");
			}

			LOGGER.debug("Persistence of the entity's data finished");

			String numEntity;
			BufferedReader br;
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				LOGGER.info("Creating an entity - Web service failed.");
				br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
			} else {
				LOGGER.info("Creating an entity- Web service worked successfully.");
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			}
			numEntity = br.readLine();
			return numEntity;

		} catch (IOException e) {
			LOGGER.error("Failed to call the Web Service", e);
			throw e;
		} finally {
			assert conn != null;
			conn.disconnect();
		}
	}

	/**
	 * This method is responsible for validate if "first_names" and "last_name" has a size lower than 40 digits.
	 * If this 40 digits were exceeded this method will not add the last name of the "first_names" set.
	 * @param doc as a struture of Document from MongoDb.
	 * @return the string containing "first_names" + "last_name" in a normalized string, that does not exceed 40 digit length.
	 */
	private String validateNameSize(Document doc) {
		String names = "";
		if ( (doc.getString("first_names") + " " + doc.getString("last_name")).length() < 40) {
			names = normalize(doc.getString("first_names").toUpperCase() + " " + doc.getString("last_name").toUpperCase());
		} else {
			String[] firstNames = doc.getString("first_names").toUpperCase().split(" ");
			for (int i = 0; i < firstNames.length-1; i++) {
				names += firstNames[i];
			}
			names = normalize(names + " " + doc.getString("last_name").toUpperCase());
		}
		return names;
	}

	/**
	 * This method creates an Client on Banka system.
	 * 
	 * @param context string that represents the "_id" of MongoDb.
	 * @throws Exception
	 */
	public void createClient(String context) throws Exception {
		try {
			Document doc = mongo.get("_id", new ObjectId(context));
			if (doc.containsKey("entityId")) {
				int entityId = doc.getInteger("entityId");
				if (!checkBloqEntity(entityId)) {
					String numCliente = null;
					if (!doc.containsKey("clientId")) {
						numCliente = callCreateClientWS(doc);
					} else {
						numCliente = String.valueOf(doc.getInteger("clientId"));
					}
					
					try {
						LOGGER.info("Client number: " + numCliente);
						int numCli = Integer.parseInt(numCliente);
						mongo.updateDoc("_id", doc.getObjectId("_id"), "clientId", numCli);
						int requestId = doc.getInteger("request_id");
						Edge edge2 = getNextEdge("ClientCreation", hibernateUtils, requestId);
						evoluteRequest(requestId, edge2.getStageByStageTo(), 5);
						createAccount(context);
						return;
					} catch (NumberFormatException e) {
						throw new RaisinException("Erro ao criar cliente: " + numCliente);
					}
				} 
				if ( doc.containsKey("clientId") ) {
					int requestId = doc.getInteger("request_id");
					Edge edge2 = getNextEdge("ClientCreation", hibernateUtils, requestId);
					evoluteRequest(requestId, edge2.getStageByStageTo(), 5);
				} else {
					throw new RaisinException("Bloqueio da entidade " + doc.get("entityId") + " mantém-se.");
				}
			} else {
				throw new RaisinException("Não foi possivel criar a entidade.");
			}
		} catch (Exception e) {
			LOGGER.error("Create a client on Banka failed.", e);
			throw e;
		}
	}

	/**
	 * This method invokes a web service to create new Client on Banka.
	 * 
	 * @param doc a MongoDb Document with structure of Client Info.
	 * @return the Client number or its error if something went wrong.
	 * @throws IOException
	 */
	private String callCreateClientWS(Document doc) throws IOException, MalformedURLException {
		HttpURLConnection conn = null;
		try {
			BankaConnection banka = BankaConnection.getInstance();
			LOGGER.info("Banka connection opened.");

			URL url = new URL("http://10.0.2.33:8080/BankaWebservices/createClient");

			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "multipart/form-data");

			LOGGER.info("Creating a client by the Web Service - (POST) at the address: " + url);

			conn.setRequestProperty("estacao", banka.getEstacao());
			conn.setRequestProperty("utilizador", banka.getUser());
			conn.setRequestProperty("balcao", "9005");
			conn.setRequestProperty("tipoTitularidade", "I");
			conn.setRequestProperty("envioCorrespondencia", "N");
			conn.setRequestProperty("codigoResidencia", "E");
			conn.setRequestProperty("codigoSectorial", "421000");
			conn.setRequestProperty("segmentoCliente", "X"); // X com Isenção
			conn.setRequestProperty("codigoPais", doc.getString("address_country"));
			conn.setRequestProperty("condicoesMovimentacao", "10");
			conn.setRequestProperty("extractoIntegrado", "N");
			conn.setRequestProperty("numeroEntidade", String.valueOf(doc.getInteger("entityId")));
			conn.setRequestProperty("relacaoCliente", "01");
			conn.setRequestProperty("gestorConta", banka.getUser());
			conn.setRequestProperty("morada", normalize(doc.getString("address_street") + " " 
					+ doc.getString("address_postal_cd") + " " + doc.getString("address_town")));

			LOGGER.debug("Persistence of the client's data finished");

			String numClient;
			BufferedReader br;
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				LOGGER.info("Creating a client - Web service failed.");
				br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
			} else {
				LOGGER.info("Creating a client - Web service worked successfully.");
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			}
			numClient = br.readLine();
			return numClient;

		} catch (IOException e) {
			LOGGER.error("Failed to call the Web Service", e);
			throw e;
		} finally {
			assert conn != null;
			conn.disconnect();
		}
	}

	/**
	 * This method creates an Account on Banka system.
	 * 
	 * @param context string that represents the "_id" of MongoDb.
	 * @throws Exception
	 */
	public void createAccount(String context) throws Exception {
		try {
			Document doc = mongo.get("_id", new ObjectId(context));
			int numCliente = doc.getInteger("clientId");
			String numAccount = null;
			if (doc.containsKey("clientId") && !doc.containsKey("accountId") ) {
				numAccount = callCreateAccountWS(numCliente);
			}  
			if ( doc.containsKey("accountId") ) {
				numAccount = String.valueOf(doc.getLong("accountId"));
			}
			
			try {
				LOGGER.info("Account number: " + numAccount);
				assert numAccount != null;
				long nConta = Long.parseLong(numAccount);
				mongo.updateDoc("_id", doc.getObjectId("_id"), "accountId", nConta);
				int requestId = doc.getInteger("request_id");
				Edge edge2 = getNextEdge("AccountCreation", hibernateUtils, requestId);
				evoluteRequest(requestId, edge2.getStageByStageTo(), 5);
			} catch (NumberFormatException e) {
				LOGGER.error("Invalid stage to move forward", e);
				throw new RaisinException("Erro ao criar conta: " + numAccount);
			}
			

		} catch (Exception e) {
			LOGGER.error("Failed to create an Account", e);
			throw e;
		}

	}

	/**
	 * This method invokes a web service to create new Account on Banka.
	 * 
	 * @param doc a MongoDb Document with structure of Account Info.
	 * @return the Account number or its error if something went wrong.
	 * @throws IOException
	 */
	private String callCreateAccountWS(int numCliente) throws IOException {
		HttpURLConnection conn = null;
		try {
			BankaConnection banka = BankaConnection.getInstance();
			LOGGER.info("Banka connection opened.");

			URL url = new URL("http://10.0.2.33:8080/BankaWebservices/createAccount");

			conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "multipart/form-data");
			conn.setRequestProperty("estacao", banka.getEstacao());
			conn.setRequestProperty("utilizador", banka.getUser());
			conn.setRequestProperty("balcao", "9005");
			conn.setRequestProperty("numCliente", String.valueOf(numCliente));
			conn.setRequestProperty("escalaJuros", "N");
			conn.setRequestProperty("moeda", "EUR");
			conn.setRequestProperty("codigoProduto", "DEP_ORDEM ");
			conn.setRequestProperty("componente", "10_10EURN ");

			String numAccount;
			BufferedReader br;
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				LOGGER.info("Creating an account - Web service failed.");
				br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
			} else {
				LOGGER.info("Creating an account - Web service worked successfully.");
				br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			}
			numAccount = br.readLine();
			return numAccount;
		} catch (IOException e) {
			LOGGER.error("Failed to create an account", e);
			throw e;
		} finally {
			assert conn != null;
			conn.disconnect();
		}
	}

	/**
	 * This method creates a PRT xml message to RAISIN.
	 * 
	 * @param doc a MongoDb Document with structure.
	 * @throws RaisinException
	 */
	public void sendPRT(String context) throws RaisinException {

		Document doc = mongo.get("_id", new ObjectId(context));
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());

		LOGGER.debug("Send PRT file started.");

		if (doc.containsKey("PYI")) {
			Document docPYI = (Document) doc.get("PYI");
			for (Entry<?, ?> docP : docPYI.entrySet()) {
				Document docPi = (Document) docP.getValue();
				Document docPRA = (Document) doc.get("PRA");
				for (Entry<?, ?> docPra : docPRA.entrySet()) {
					Document docPraValue = (Document) docPra.getValue();
					if (docPraValue.getString("maturing_term_deposit_id").equals(docPi.getString("term_deposit_id"))
							&& docPi.getLong("accountDp").compareTo(docPraValue.getLong("accountDp")) == 0) {
						if (!managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
							throw new RaisinException("O sistema encontra-se em fecho.");
						}
						List<Long> moveList = managerDB.getDpMovementList(docPi.getLong("accountDp"), 742);
						if (moveList.size() != 0) { // já houve pelo menos uma renovação de DP
							Long date = Collections.max(moveList);
							if (!docPraValue.containsKey("renewalDate") || (docPraValue.containsKey("renewalDate")
									&& !date.equals(docPraValue.getLong("renewalDate")))) {
								generateXml(docPi, "PRT", "");
								generateXml(doc, "PRC", "");
								mongo.updateDoc("_id", doc.getObjectId("_id"), "PRA." + docPra.getKey() + ".renewalDate",
										date);
								int requestId = doc.getInteger("request_id");
								Edge edge2 = getNextEdge("sendPRT", hibernateUtils, requestId);
								evoluteRequest(requestId, edge2.getStageByStageTo(), 5);
								LOGGER.debug("Send PRT file finished successfully.");
							} else { // esta renovação da Banka já foi tratada
								LOGGER.warn("Esta renovação da Banka já foi tratada, para o DP = " + docPi.getLong("accountDp"));
							}
						} else { // não houve ainda uma renovação na Banka
							LOGGER.warn("Não houve ainda uma renovação na Banka, para o DP = " + docPi.getLong("accountDp"));
						}
					}
				}
			}
		}

	}

	/**
	 * This method verifies
	 * 
	 * @param banka     as the Banka connection that make
	 * @param managerDB
	 * @param docPi
	 * @return
	 * @throws RaisinException
	 * @throws ParseException
	 */
	private boolean isDpEnded(BankaConnection banka, Manager managerDB, Document docPi)
			throws RaisinException, ParseException {
		String endDateDp = getDtEndDp(docPi.getLong("accountDp"));
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		Date dateFormated = formatter.parse(endDateDp);

		Calendar endDpDate = Calendar.getInstance();
		endDpDate.setTime(dateFormated);

		Calendar nowDate = Calendar.getInstance();
		if (!managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
		nowDate.setTime(formatter.parse(managerDB.getDataBanka()));
		return endDpDate.get(Calendar.DAY_OF_YEAR) <= nowDate.get(Calendar.DAY_OF_YEAR)
				&& endDpDate.get(Calendar.YEAR) == nowDate.get(Calendar.YEAR);

	}

	public void generateXml(Document doc, String type, String message) {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		LOGGER.info("generateXml type: " + type + " for doc: " + doc.getString("term_deposit_id"));
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			org.w3c.dom.Document docXml = dBuilder.newDocument();

			Element rootElement = docXml.createElement("record");
			docXml.appendChild(rootElement);

			Element header = docXml.createElement("header");
			rootElement.appendChild(header);

			Element recordType = docXml.createElement("record-type-cd");
			recordType.setTextContent(type);
			header.appendChild(recordType);

			Element protocol = docXml.createElement("record-protocol-version");
			protocol.setTextContent(doc.getString("record_protocol_version"));
			header.appendChild(protocol);

			Element recordCreationDate = docXml.createElement("record-creation-dt");
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			String date = simpleDateFormat.format(new Date());
			recordCreationDate.setTextContent(date);
			header.appendChild(recordCreationDate);

			Element servicing = docXml.createElement("servicing-bank-swift-cd");
			servicing.setTextContent(doc.getString("servicing_bank_swift_cd"));
			header.appendChild(servicing);

			Element partner = docXml.createElement("partner-bank-swift-cd");
			partner.setTextContent(doc.getString("partner_bank_swift_cd"));
			header.appendChild(partner);

			Element term = docXml.createElement("term-deposit-id");
			term.setTextContent(doc.getString("term_deposit_id"));
			header.appendChild(term);

			String outputPath = null;
			switch (type) {
			case "ACR": // account created
				Element account = docXml.createElement("customer-account-iban");
				String iban = getIBAN(String.valueOf(doc.get("accountId")));
				account.setTextContent(iban);
				rootElement.appendChild(account);
				outputPath = "/opt/raisin/data/output/opening/";
				break;
			case "OPR": // reject
				Element rejectCase = docXml.createElement("rejection-case");
				rejectCase.setTextContent("");
				rootElement.appendChild(rejectCase);
				Element rejectText = docXml.createElement("rejection-text");
				rejectText.setTextContent(message);
				rootElement.appendChild(rejectText);
				simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String rejectDateString = simpleDateFormat.format(new Date());
				Element rejectDate = docXml.createElement("rejection-dt");
				rejectDate.setTextContent(rejectDateString);
				rootElement.appendChild(rejectDate);
				outputPath = "/opt/raisin/data/output/opening/";
				break;
			case "OPC": // accountDP created
				fillOpcFile(doc, docXml, rootElement);
				outputPath = "/opt/raisin/data/output/placement/";
				break;
			case "PRT": // accountDP renewal
				fillPyoFile(doc, docXml, rootElement, 742); // the content of PRT file is the same of PYO file.
				outputPath = "/opt/raisin/data/output/placement/";
				break;
			case "PYO":
				fillPyoFile(doc, docXml, rootElement, 600);
				outputPath = "/opt/raisin/data/output/placement/";
				break;
			case "PRC":
				fillPrcFile(doc, docXml, rootElement);
				outputPath = "/opt/raisin/data/output/placement/";
				break;
			default:
				break;
			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(docXml);
			
			String fileName = "";
			if(doc.containsKey("PRA")) {
				Document docPyi = (Document) doc.get("PYI");
				Document docPra = (Document) doc.get("PRA");
				for (Object docP : docPyi.values()) {
					Document docPyiValue = (Document) docP;
					for (Object docPraKV : docPra.values()) {
						Document docPraValue = (Document) docPraKV;
						if (docPraValue.containsKey("accountDp")
								&& docPraValue.getLong("accountDp").compareTo(docPyiValue.getLong("accountDp")) == 0) {
							NodeList eleHeader = docXml.getElementsByTagName("header");
							for (int i = 0; i < eleHeader.getLength(); i++) {
								NodeList eleHeaderChilds = eleHeader.item(i).getChildNodes();
								for (int j = 0; j < eleHeaderChilds.getLength(); j++) {
									if ("term-deposit-id".equals(eleHeaderChilds.item(j).getNodeName())) {
										fileName = docPraValue.getString("term_deposit_id");
									}
								}
							}
						}
					}
				}
			} else {
				fileName = doc.getString("term_deposit_id");
			}
			StreamResult result = new StreamResult(new File(outputPath + fileName + "-" + type + ".xml"));
			transformer.transform(source, result);

		} catch (Exception e) {
			LOGGER.error("generateXml", e);
		}

	}

	private void fillOpcFile(Document doc, org.w3c.dom.Document docXml, Element rootElement) {
		Element placementExchangeRate = docXml.createElement("placement-exchange-rate");
		placementExchangeRate.setTextContent("1.000");
		rootElement.appendChild(placementExchangeRate);
		Element placementExchangeDate = docXml.createElement("placement-exchange-dt");
		String receptionDate = doc.getString("receptionDate");
		placementExchangeDate.setTextContent(receptionDate);
		rootElement.appendChild(placementExchangeDate);
		Element placementAmount = docXml.createElement("placement-amount");
		String amount = doc.getString("ordered_amount");
		placementAmount.setTextContent(amount);
		rootElement.appendChild(placementAmount);
		Element placementRate = docXml.createElement("placement-interest-rate");
		String rate = doc.getString("order_opening_interest_rate");
		placementRate.setTextContent(rate);
		rootElement.appendChild(placementRate);

		Element startDt = docXml.createElement("placement-start-dt");
		String startDate = doc.getString("receptionDate");
		startDt.setTextContent(startDate);
		rootElement.appendChild(startDt);

		try {
			Element endDate = docXml.createElement("placement-maturity-dt");
			String date = getDtEndDp(doc.getLong("accountDp"));
			DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			Date dateFormated = formatter.parse(date);
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String dateF = formatter.format(dateFormated);
			endDate.setTextContent(dateF);
			rootElement.appendChild(endDate);
		} catch (Exception e) {
			LOGGER.error("generateXml", e);
		}
		Element docName = docXml.createElement("opening-confirmation-document-name");
		rootElement.appendChild(docName);
	}

	private void fillPyoFile(Document doc, org.w3c.dom.Document docXml, Element rootElement, int copeNr) throws RaisinException, ParseException {
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (!managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
		List<BigDecimal> moveList;
		List<Long> dtMoveList;
		if ( copeNr == 742 ) {
			moveList = managerDB.getDpEndDPMovement(doc.getLong("accountDp"));
			dtMoveList = managerDB.getDpMovementList(doc.getLong("accountDp"), copeNr);
		} else {
			moveList = managerDB.getEndDpMovement(doc.getLong("accountDp"), copeNr);
			dtMoveList = managerDB.getDpMovementList(doc.getLong("accountDp"), copeNr);
		}
		Long date = Collections.max(dtMoveList);
		DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		Date dateFormated = formatter.parse(String.valueOf(date));
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateF = formatter.format(dateFormated);
		
		BigDecimal orderedAmount = new BigDecimal(doc.getString("ordered_amount"));
		BigDecimal juros = BigDecimal.ZERO, impostos = BigDecimal.ZERO;
		for (BigDecimal movementValue : moveList) {
			if ( movementValue.compareTo(orderedAmount) != 0 && movementValue.compareTo(BigDecimal.ZERO) > 0 ) {
				juros = movementValue;
			} else if ( movementValue.compareTo(orderedAmount) != 0 ) {
				impostos = movementValue;
			}
		}
		juros = juros.subtract(impostos.abs());
		orderedAmount.setScale(2);
		juros.setScale(2);
		impostos.setScale(2);

		Element placementAmount = docXml.createElement("payout-amount");
		placementAmount.setTextContent(doc.getString("amount") == null ? "0.00" : doc.getString("amount"));
		rootElement.appendChild(placementAmount);
		Element payoutInterests = docXml.createElement("payout-interests");
		payoutInterests.setTextContent(String.valueOf(juros));
		rootElement.appendChild(payoutInterests);
		Element sourceTax = docXml.createElement("source-tax");
		sourceTax.setTextContent(String.valueOf(impostos.abs()));
		rootElement.appendChild(sourceTax);
		Element placementExchangeRate = docXml.createElement("payout-exchange-rate");
		placementExchangeRate.setTextContent("1.00");
		rootElement.appendChild(placementExchangeRate);
		Element payoutExchangeDt = docXml.createElement("payout-exchange-dt");
		payoutExchangeDt.setTextContent(dateF);
		rootElement.appendChild(payoutExchangeDt);
		Element convertedPayoutAmount = docXml.createElement("converted-payout-amount");
		convertedPayoutAmount.setTextContent(doc.getString("amount") == null ? "0.00" : doc.getString("amount"));
		rootElement.appendChild(convertedPayoutAmount);
		Element localPayout = docXml.createElement("local-payout");
		localPayout.setTextContent("False");
		rootElement.appendChild(localPayout);
		Element payoutDt = docXml.createElement("payout-dt");
		payoutDt.setTextContent(dateF);
		rootElement.appendChild(payoutDt);
		Element cancellationText = docXml.createElement("cancellation-text");
//		cancellationText.setTextContent("1.000");
		rootElement.appendChild(cancellationText);

	}

	/**
	 * This method is responsible for fill the specific part of PRC file.
	 * @param doc as the structure of this request
	 * @param docXml as the xml document to append the information
	 * @param rootElement as the root element of xml file
	 * @throws DOMException
	 * @throws RaisinException
	 */
	private void fillPrcFile(Document doc, org.w3c.dom.Document docXml, Element rootElement)
			throws DOMException, RaisinException {
		long accountDo = doc.getLong("accountId");
		Document docPyi = (Document) doc.get("PYI");
		Document docPra = (Document) doc.get("PRA");
		for (Object docP : docPyi.values()) {

			Document docPyiValue = (Document) docP;
			Element customerAccountIban = docXml.createElement("customer-account-iban");
			customerAccountIban.setTextContent(getIBAN(String.valueOf(accountDo)));
			rootElement.appendChild(customerAccountIban);
			Element termDepositReference = docXml.createElement("term-deposit-reference");
			rootElement.appendChild(termDepositReference);
			Element placementAmount = docXml.createElement("prolongation-amount");
			placementAmount.setTextContent(String.valueOf(getDpAccountBalance(docPyiValue.getLong("accountDp"))));
			rootElement.appendChild(placementAmount);
			Element placementRate = docXml.createElement("prolongation-interest-rate");
			String rate = docPyiValue.getString("order_opening_interest_rate");
			placementRate.setTextContent(rate);
			rootElement.appendChild(placementRate);

			Element terminationInterestRate = docXml.createElement("termination-interest-rate");
			rootElement.appendChild(terminationInterestRate);
			
			BankaConnection banka = BankaConnection.getInstance();
			Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
			if (!managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
				throw new RaisinException("O sistema encontra-se em fecho.");
			}
			List<Long> dtMoveList = managerDB.getDpMovementList(docPyiValue.getLong("accountDp"), 742);
			Long date = Collections.max(dtMoveList);
			DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			try {
				Date dateFormated = formatter.parse(String.valueOf(date));
				formatter = new SimpleDateFormat("yyyy-MM-dd");
				String dateF = formatter.format(dateFormated);

				Element prolongationStartDt = docXml.createElement("prolongation-start-dt");
				prolongationStartDt.setTextContent(dateF);
				rootElement.appendChild(prolongationStartDt);

				Element endDate = docXml.createElement("prolongation-maturity-dt");
				String dateStr = getDtEndDp(docPyiValue.getLong("accountDp"));
				formatter = new SimpleDateFormat("yyyyMMdd");
				dateFormated = formatter.parse(dateStr);
				formatter = new SimpleDateFormat("yyyy-MM-dd");
				dateF = formatter.format(dateFormated);
				endDate.setTextContent(dateF);
				rootElement.appendChild(endDate);
			} catch (Exception e) {
				LOGGER.error("generateXml", e);
			}
			
			for (Object docPraKV : docPra.values()) {
				Document docPraValue = (Document) docPraKV;
				if ( docPraValue.containsKey("accountDp") && docPraValue.getLong("accountDp").compareTo(docPyiValue.getLong("accountDp")) == 0 ) {
					NodeList eleHeader = docXml.getElementsByTagName("header");
					for (int i = 0; i < eleHeader.getLength(); i++) {
						NodeList eleHeaderChilds = eleHeader.item(i).getChildNodes();
						for (int j = 0; j < eleHeaderChilds.getLength(); j++) {
							if (  "term-deposit-id".equals(eleHeaderChilds.item(j).getNodeName()) ) {
								eleHeaderChilds.item(j).setTextContent(docPraValue.getString("term_deposit_id"));
							}
							
						}
						
					}
					
					
				}
			}
		}
	}

	public static String normalize(String input) {
		if (input != null) {
			return Normalizer.normalize(input, Normalizer.Form.NFD)
				.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
					.replaceAll("[^\\p{ASCII}]", " ");
		}
		return "";
	}

	public boolean hasFunds(Document doc) throws RaisinException {
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			Document docPYI = (Document) doc.get("PYI");
			for (Object docP : docPYI.values()) {
				if (!((Document) docP).containsKey("accountDp")) {
					Document docPyiValue = (Document) docP;
					BigDecimal dpValue = new BigDecimal(docPyiValue.getString("ordered_amount"));
					String nib = docPyiValue.getString("customer_account_iban").substring(4);
					ResultContacliente resultContacliente = managerDB.getAccountNIB(nib);
					LOGGER.info("AccountNr has funds? : " + (dpValue.compareTo(resultContacliente.getSaldo()) <= 0));
					return dpValue.compareTo(resultContacliente.getSaldo()) <= 0;
				}
			}
			return false;
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
	}

	public String getDtEndDp(long dpAccountNum) throws RaisinException {
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			return managerDB.getDtEndDp(dpAccountNum);
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}

	}

	public boolean checkBloqEntity(int entityId) throws RaisinException {
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			boolean entityExists = managerDB.getEntBloqAML(entityId);
			LOGGER.info("Entity " + entityId + " has a lock? : " + entityExists);
			managerDB.closeAll();
			return entityExists;
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
	}

	private String getIBAN(String account) throws RaisinException {
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			ClientAccount clientAccount = managerDB.getClientFromInput(account);
			LOGGER.info("AccountNr " + account + " has IBAN = " + clientAccount.getAccountIBAN());
			managerDB.closeAll();
			return clientAccount.getAccountIBAN();
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
	}

	public void checkClient(Document doc) throws RaisinException {
		int entityNr = doc.getInteger("entityId");
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			Integer clientNr = managerDB.getNrClienteByEntityNr(entityNr);
			LOGGER.info("entityNr " + entityNr + " has clientNr = " + clientNr);
			managerDB.closeAll();

			if (clientNr != null && doc.getInteger("clientId") == 0) {
				mongo.updateDoc("_id", doc.getObjectId("_id"), "clientId", clientNr);
				LOGGER.info("Entity nr: " + entityNr + "\t with ID = " + doc.getObjectId("_id") + " updated clientNr = "
						+ clientNr);
			} else {
				LOGGER.info("clientNr already updated!");
			}

		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
	}

	public void checkAccount(Document doc) throws RaisinException {
		int clientNr = doc.getInteger("clientId");
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			Long accountId = managerDB.getDoByClientNr(clientNr);
			LOGGER.info("clientNr " + clientNr + " has accountId = " + accountId);
			managerDB.closeAll();
			if (accountId != null && doc.getLong("accountId") == 0l) {
				mongo.updateDoc("_id", doc.getObjectId("_id"), "accountId", accountId);
				LOGGER.info("client nr: " + clientNr + "\t with ID = " + doc.getObjectId("_id")
						+ " updated accountId = " + accountId);
			} else {
				LOGGER.info("AccontNr already updated!");
			}
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
	}

	public BigDecimal getDpAccountBalance(long accountDp) throws RaisinException {
		BankaConnection banka = BankaConnection.getInstance();
		Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
		if (managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
			return managerDB.getDpBalance(accountDp);
		} else {
			throw new RaisinException("O sistema encontra-se em fecho.");
		}
	}

	/**
	 * This method verifies if the SEPA was approved and update doc with operation
	 * number.
	 * 
	 * @param context, string with '_id'
	 * @throws RaisinException in case of Banka system is unavailable.
	 * @throws ParseException  in case of an problem occurred in date parsing.
	 */
	public void checkSepa(String context) throws RaisinException, ParseException {
		Document doc = mongo.get("_id", new ObjectId(context));
		Long accountId = doc.getLong("accountId");
		if (doc.containsKey("PYI")) {
			Document docPYI = (Document) doc.get("PYI");
			for (Entry<?, ?> docP : docPYI.entrySet()) {
				Document docPi = (Document) docP.getValue();
				BankaConnection banka = BankaConnection.getInstance();
				Manager managerDB = new Manager(banka.getRelease(), banka.getLibVersion());
				if (!managerDB.startUp(banka.getUser(), banka.getPassword(), banka.getHostname())) {
					throw new RaisinException("O sistema encontra-se em fecho.");
				}
				if (isDpEnded(banka, managerDB, docPi)) {
					Long accountDp = docPi.getLong("accountDp");
					String destIban = docPi.getString("transaction_account_iban");
					PairWithGenerics<Long, BigDecimal> pair = managerDB.getSepaNumber("I", accountId, destIban,
							String.valueOf(accountDp));

					if (pair != null) {
						Long numOper = pair.getKey();
						BigDecimal amount = pair.getValue();

						LOGGER.debug("accountDp = " + accountDp + " has sent a SEPA with operId = " + numOper
								+ "\t and amount = " + amount);
						managerDB.closeAll();
						if (numOper != null && numOper != 0L) {
							mongo.updateDoc("_id", doc.getObjectId("_id"), "PYI." + docP.getKey() + ".numTransf",
									numOper);
							mongo.updateDoc("_id", doc.getObjectId("_id"), "PYI." + docP.getKey() + ".amount",
									String.valueOf(amount));
						} else {
							LOGGER.warn("ALREADY UPDATE ON:\naccountDp = " + accountDp
									+ " has sent a SEPA with operId = " + numOper + " and amount = " + amount);
						}
					} else {
						LOGGER.warn("SEPA not found.");
					}
				}
			}
		}

	}

}
