package pt.bai.raisin.db.postgres.entities.workflow;
// Generated Jul 13, 2018 11:23:01 AM by Hibernate Tools 4.3.5.Final

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Stage generated by hbm2java
 */
@Entity
@Table(name = "stage", schema = "workflow", uniqueConstraints = @UniqueConstraint(columnNames = { "workflow_id",
		"name" }))
public class Stage implements java.io.Serializable {

	private static final long serialVersionUID = 689026899075925617L;
	
	private int id;
	private Workflow workflow;
	private String name;
	private Date creationDate;
	private String function;
	private String label;
	private Set<History> histories = new HashSet<History>(0);
	private Set<Position> positions = new HashSet<Position>(0);
	private Set<Edge> edgesForStageTo = new HashSet<Edge>(0);
	private Set<Edge> edgesForStageFrom = new HashSet<Edge>(0);

	public Stage() {
	}

	public Stage(int id, Workflow workflow, String name) {
		this.id = id;
		this.workflow = workflow;
		this.name = name;
	}

	public Stage(int id, Workflow workflow, String name, Date creationDate, String function, String label,
			Set<History> histories, Set<Position> positions, Set<Edge> edgesForStageTo, Set<Edge> edgesForStageFrom) {
		this.id = id;
		this.workflow = workflow;
		this.name = name;
		this.creationDate = creationDate;
		this.function = function;
		this.label = label;
		this.histories = histories;
		this.positions = positions;
		this.edgesForStageTo = edgesForStageTo;
		this.edgesForStageFrom = edgesForStageFrom;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workflow_id", nullable = false)
	public Workflow getWorkflow() {
		return this.workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date", length = 29)
	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name = "function", length = 250)
	public String getFunction() {
		return this.function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	@Column(name = "label", length = 25)
	public String getLabel() {
		return this.label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stage")
	public Set<History> getHistories() {
		return this.histories;
	}

	public void setHistories(Set<History> histories) {
		this.histories = histories;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stage")
	public Set<Position> getPositions() {
		return this.positions;
	}

	public void setPositions(Set<Position> positions) {
		this.positions = positions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stageByStageTo")
	public Set<Edge> getEdgesForStageTo() {
		return this.edgesForStageTo;
	}

	public void setEdgesForStageTo(Set<Edge> edgesForStageTo) {
		this.edgesForStageTo = edgesForStageTo;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stageByStageFrom")
	public Set<Edge> getEdgesForStageFrom() {
		return this.edgesForStageFrom;
	}

	public void setEdgesForStageFrom(Set<Edge> edgesForStageFrom) {
		this.edgesForStageFrom = edgesForStageFrom;
	}

}
