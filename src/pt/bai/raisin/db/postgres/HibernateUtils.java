package pt.bai.raisin.db.postgres;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class HibernateUtils {

	private static HibernateUtils instance;
	private EntityManagerFactory factory;
	private EntityManager entityManager;

	protected HibernateUtils() {
		init();
	}

	public static HibernateUtils getInstance() {
		if (instance == null) {
			instance = new HibernateUtils();
			if (!instance.getEntityManager().isOpen()) {
				instance.factory.createEntityManager();
			}
		}
		return instance;
	}

	private void init() {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("/opt/raisin/config/postgres.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		factory = Persistence.createEntityManagerFactory("postgres", props);
	}


	public EntityManager getEntityManager() {
		if( entityManager == null || !entityManager.isOpen() ) {
			entityManager = factory.createEntityManager();
		}
		return entityManager;
	}

	public void closeEntityManager() {
		if (entityManager != null) {
			entityManager.close();
		}
	}

	public void close() {
		if (factory != null) {
			factory.close();
		}
	}
}

