package pt.bai.raisin.db.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.model.GridFSUploadOptions;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import pt.bai.sp.outputoperator.config.ConfigUtils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MongoConnection {
	
	private static MongoConnection instance = null;
	private MongoClient mongoClient;
	private MongoDatabase db;
	private MongoCollection<Document> collection;
	private GridFSBucket gridFSBucket;
	

	protected MongoConnection() {
		connect();
	}
	
	public static MongoConnection getInstance() {
		if (instance == null) {
			instance = new MongoConnection();
		}
		return instance;
	}
	
	private void connect() {
		ConfigUtils configUtils = new ConfigUtils();
//		String path = MongoConnection.class.getProtectionDomain().getCodeSource().getLocation().getPath();
//		String pathFile = null;
//		try {
//			String decodedPath = URLDecoder.decode(path, "UTF-8");
//			pathFile = decodedPath.substring(0, path.lastIndexOf("/") + 1);
//		} catch (UnsupportedEncodingException e) {
//			 System.err.println(e.getMessage() +  e);
//		}

		final Properties config = configUtils.loadProps("/opt/raisin/config/mongo.properties");
		final int port = Integer.parseInt(config.getProperty("mongo.port"));
		final String user = config.getProperty("mongo.user");
		final String password = config.getProperty("mongo.password");
		final String host = config.getProperty("mongo.host");
		final String database = config.getProperty("mongo.database");
		final String collectionName = config.getProperty("mongo.collection");

		MongoCredential mongoAuth = MongoCredential.createCredential(user, database, password.toCharArray());
		ServerAddress serverAddress = new ServerAddress(host, port);
		List<MongoCredential> auths = new ArrayList<MongoCredential>();
		auths.add(mongoAuth);
		mongoClient = new MongoClient(serverAddress, auths);

		db = mongoClient.getDatabase(database);
		
		collection = db.getCollection(collectionName);

		// Create a gridFSBucket with a custom bucket name "BgpFiles"
		gridFSBucket = GridFSBuckets.create(db, "RaisinFiles");

	}

	public MongoClient getMongoClient() {
		return mongoClient;
	}

	public MongoDatabase getDb() {
		return db;
	}

	public Document getValueFromAnotherCollection( String collectionName, String key, String value) {
		MongoCollection<Document> collection = db.getCollection(collectionName);
		return collection.find(Filters.eq(key, value)).first();
	}

	public MongoCollection<Document> getCollection() {
		return collection;
	}
	
	public void insertOne(Document doc) {
		collection.insertOne(doc);
	}

	public void insertMany(List<Document> docList) {
		collection.insertMany(docList);
	}

	public Document get(String key, Object value) {
		return collection.find(Filters.eq(key, value)).first();
	}
	
	public Document getWithAnd(List<Bson> filterList) {
		return collection.find(Filters.and( filterList )).first();
	}
	
	public Document getWithOr(List<Bson> filterList) {
		return collection.find(Filters.or( filterList )).first();
	}
	
	public MongoCursor<Document> getAll(String key, Object value) {
		return collection.find(Filters.eq(key, value)).iterator();
	}
	
	public void updateDoc(String key, Object value, String keyToUpdate, Object valueToUpdate) {
		collection.updateOne(Filters.eq(key, value), new Document("$set", new Document(keyToUpdate, valueToUpdate)));
	}
	
	public ObjectId saveInputStreamFile(InputStream inputStream, String fileName, ObjectId id ) {

		String json = "{\"idProcesso\": \"" + id.toString() + "\" }"; // + ", \"tipo\":\"" + tipo + "\"}";
		Document document = Document.parse(json);

		// Create some custom options
		GridFSUploadOptions options = new GridFSUploadOptions().chunkSizeBytes(1024).metadata(document);

		return gridFSBucket.uploadFromStream(fileName, inputStream, options);
	}
	/*
	public List<FileWrapper> getFile(int searchId, String tipo) throws UnsupportedEncodingException {

		BasicDBObject idFilter = new BasicDBObject("metadata.idInterno", searchId);
		BasicDBObject tipoFilter = new BasicDBObject("metadata.tipo", tipo);
       
		final List<FileWrapper> fileList = new ArrayList<FileWrapper>();
	
		gridFSBucket.find(Filters.and(idFilter, tipoFilter)).forEach(new Block<GridFSFile>() {
			@Override
			public void apply(final GridFSFile file) {
				GridFSDownloadStream downloadStream = gridFSBucket.openDownloadStream(file.getId());
				int fileLength = (int) file.getLength();
				byte[] bytesToWriteTo = new byte[fileLength];
				downloadStream.read(bytesToWriteTo);
				downloadStream.close();

				String mimeType = "application/zip";
				InputStream targetStream = new ByteArrayInputStream(bytesToWriteTo);
				
				fileList.add(new FileWrapper(file.getId().asObjectId().getValue(), downloadStream.getGridFSFile().getFilename(),
						mimeType, targetStream));
			}
		});
					
		return fileList;
	}
	
	public void updateFileMetadata(String mongoId, String userApagou, int id, String tipo) {

		String json = "{\"metadata.idInterno\":" + id + ", \"metadata.tipo\":\"" + tipo + "_APAGADO\",  \"metadata.userApagou\": \"" + userApagou + "\"}";
		final Document document = Document.parse(json);

		BasicDBObject idFilter = new BasicDBObject("_id", new ObjectId(mongoId));
		MongoCollection<Document> collection = db.getCollection("BgpFiles.files");
		if( collection.find(idFilter).first() != null ) {
			collection.updateOne(idFilter, new Document("$set", document));
		}
	}
	*/
	public void disconnect() {
		if (mongoClient != null) {
			mongoClient.close();
		}
	}
}