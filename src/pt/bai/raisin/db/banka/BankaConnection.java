package pt.bai.raisin.db.banka;

import pt.bai.sp.outputoperator.config.ConfigUtils;

import java.util.Properties;

public class BankaConnection {
	
	private static BankaConnection instance = null;
	
	private String hostname;
	private String user;
	private String password;
	private String libVersion;
	private String estacao;
	private String release;

	protected BankaConnection() {
		ConfigUtils configUtils = new ConfigUtils();
//		String path = BankaConnection.class.getProtectionDomain().getCodeSource().getLocation().getPath();
//		String pathFile = null;
//		try {
//			String decodedPath = URLDecoder.decode(path, "UTF-8");
//			pathFile = decodedPath.substring(0, path.lastIndexOf("/") + 1);
//		} catch (UnsupportedEncodingException e) {
//			 System.err.println(e.getMessage() +  e);
//		}
		
		Properties config = configUtils.loadProps("/opt/raisin/config/banka.properties");
		hostname = config.getProperty("hostname");
		user = config.getProperty("user");
		password = config.getProperty("pass");
		libVersion = config.getProperty("version");
		estacao = config.getProperty("estacao");
		release = config.getProperty("release");
		
	}

	public static BankaConnection getInstance() {
		if (instance == null) {
			instance = new BankaConnection();
		}
		return instance;
	}

	public String getHostname() {
		return hostname;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getLibVersion() {
		return libVersion;
	}

	public String getEstacao() {
		return estacao;
	}

	public String getRelease() {
		return release;
	}

}