package pt.bai.raisin.mw;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import pt.bai.raisin.RaisinImpl;
import pt.bai.raisin.db.mongo.MongoConnection;
import pt.bai.raisin.db.postgres.HibernateUtils;
import pt.bai.raisin.db.postgres.entities.workflow.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;


public class Creator {

	private final static Logger LOGGER = Logger.getLogger(Creator.class);

	private HibernateUtils hibernateUtils;
	
	public static void main(String[] args) {
		PropertyConfigurator.configure("log4jMW.properties");

		// String pathBase = "/home/rui/raisin/";
		String pathBase = "/opt/raisin/data/input/";

		String pathPlacement = pathBase + "placement", typePYI = "PYI";
		String pathMAT = pathBase + "maturity", typeMAT = "MAT";
		String pathPRA = pathBase + "prolongation", typePRA = "PRA";

		LOGGER.info("Raisin Middleware started.");
		
		Creator createEntity = new Creator();
		try {
			createEntity.readFileInput(); // reads OPA files.
		} catch (Exception e) {
			LOGGER.error("Creator OPA files", e);
		}
		// createEntity.readFilePlacement();
		try {
			createEntity.readFileGeneric(pathPlacement, typePYI);
		} catch (Exception e) {
			LOGGER.error("Creator PYI files", e);
		}
		try {
			createEntity.readFileGeneric(pathMAT, typeMAT);
		} catch (Exception e) {
			LOGGER.error("Creator MAT files", e);
		}
		try {
			createEntity.readFileGeneric(pathPRA, typePRA);
		} catch (Exception e) {
			LOGGER.error("Creator PRA files", e);
		}

		LOGGER.info("Raisin Middleware finished successfully.");
//		createEntity.hibernateUtils.close();
	}

	private HashMap<String, String> processXML(String xml)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		Document doc = dBuilder.parse(is);
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("record");

		HashMap<String, String> map = new HashMap<String, String>();

		for (int temp = 0; temp < nList.getLength(); temp++) {

			Node nNode = nList.item(temp);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				NodeList nodeList = nNode.getChildNodes();
				for (int k = 0; k < nodeList.getLength(); k++) {
					// To ignore this tag TODO: find a way to avoid this.
					if (!"#text".equals(nodeList.item(k).getNodeName())
							&& nodeList.item(k).getNodeName().length() > 0) {
						processTag(nodeList.item(k), map);
					}
				}
			}
		}

		return map;
	}

	public void processTag(Node nodes, HashMap<String, String> map) {
		HashMap<Integer, String> hashQuestions = new HashMap<>();
		Integer key = null;
		String value;

		if (nodes.getChildNodes().getLength() > 1) {
			NodeList nodeList = nodes.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				if (!"#text".equals(nodeList.item(i).getNodeName())) {
					if ("kyc-question".equals(nodeList.item(i).getNodeName())) {
						NodeList nodeQuest = nodeList.item(i).getChildNodes();
						for (int j = 0; j < nodeQuest.getLength(); j++) {
							if ("id".equals(nodeQuest.item(j).getNodeName())) {
								key = Integer.valueOf(nodeQuest.item(j).getTextContent());
							} else if ("answers".equals(nodeQuest.item(j).getNodeName())) {
								NodeList nodeAnswers = nodeQuest.item(j).getChildNodes();
								for (int k = 0; k < nodeAnswers.getLength(); k++) {
//									NodeList nodeAnswers1 = nodeAnswers.item(k).getChildNodes();
//									for (int l = 0; l < nodeAnswers1.getLength(); l++) {
//										if (key.intValue() == 2 || key.intValue() == 4) {
//											if ("display-text".equals(nodeAnswers1.item(l).getNodeName())) {
//												value = nodeAnswers1.item(l).getTextContent();
//												hashQuestions.put(key, value);
//											}
//										} else {
//											if ("id".equals(nodeAnswers1.item(l).getNodeName())) {
//												value = nodeAnswers1.item(l).getTextContent();
//												hashQuestions.put(key, value);
//											}
//										}
//									}

									if (key.intValue() == 2 || key.intValue() == 4) {
										if ("display-text".equals(nodeAnswers.item(k).getNodeName())) {
											value = nodeAnswers.item(k).getTextContent();
											hashQuestions.put(key, value);
										}
									} else {
										if ("id".equals(nodeAnswers.item(k).getNodeName())) {
											value = nodeAnswers.item(k).getTextContent();
											hashQuestions.put(key, value);
										}
									}
									
								}
							}
						}
					} else { // header
						map.put(nodeList.item(i).getNodeName().replaceAll("-", "_"), nodeList.item(i).getTextContent());
					}
				}
			}

		} else {
			map.put(nodes.getNodeName().replaceAll("-", "_"), nodes.getTextContent());
		}
		for (Entry<Integer, String> entry : hashQuestions.entrySet()) {
			switch (entry.getKey()) {
			case 1:
				map.put("education", entry.getValue());
				break;
			case 2:
				map.put("employer", entry.getValue());
				break;
			case 3:
				map.put("industry", entry.getValue());
				break;
			// case 4:
			// map.put("otherNationalities", entry.getValue());
			// break;
			case 5:
				map.put("profession", entry.getValue());
				break;

			default:
				map.put(entry.getKey().toString(), entry.getValue());
				break;
			}

		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		map.put("receptionDate", simpleDateFormat.format(new Date()));
	}

	private void readFileInput() {
		String pathInput = "/opt/raisin/data/input/opening";
		// String pathInput = "/home/rui/opening";
		hibernateUtils = HibernateUtils.getInstance();
		File folder = new File(pathInput);
		File[] listOfFiles = folder.listFiles();
		LOGGER.debug("Creator process started.");
		if (listOfFiles != null) {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					LOGGER.info(i + " = " + listOfFiles[i].getName());
				} else {
					if (!"OLD".equals(listOfFiles[i].getName()) && !"ERROR".equals(listOfFiles[i].getName())) {
						LOGGER.info(i + " =isDir= " + listOfFiles[i].getName());
						if (isNewFile(listOfFiles[i].getName())) {
							LOGGER.info("Found a new file!\t" + listOfFiles[i].getName());

							String xml = readFile(
									listOfFiles[i].listFiles((dir, name) -> name.toLowerCase().endsWith(".xml"))[0]);
							ObjectId objId = processFile(xml);
							saveFilesOnMongo(
									listOfFiles[i].listFiles((dir, name) -> !name.toLowerCase().endsWith(".xml")),
									objId);
							sendEmail("Novo pedido RAISIN",
									"Recebemos um novo pedido na nossa plataforma com o seguinte id:\n\n" + listOfFiles[i].getName(), "novoCliente");
							try {
								FileUtils.moveDirectory(new File(pathInput + "/" + listOfFiles[i].getName()),
										new File(pathInput + "/OLD/" + listOfFiles[i].getName()));
							} catch (IOException e) {
								LOGGER.error("Failed to move file to '/OLD/' .", e);
							}
						} else {
							LOGGER.info("Found a repetition!\t" + listOfFiles[i].getName());
							sendEmail("Novo cliente RAISIN em erro",
									"Um novo ficheiro de integração foi detectado como possivel repetição.\n\n" + pathInput + "/ERROR/"
											+ listOfFiles[i].getName(), "error");
							try {
								FileUtils.moveDirectory(new File(pathInput + "/" + listOfFiles[i].getName()),
										new File(pathInput + "/ERROR/" + listOfFiles[i].getName()));
							} catch (IOException e) {
								LOGGER.error("readFileInput - Found a repetition! - ERROR move", e);
							}
						}

					}
				}
			}
		}
		LOGGER.debug("Creator process ended.");

	}

	private void sendEmail(String subject, String bodyMsg, String type) {
		
		Properties props = new Properties();
		try (InputStream inputStream = new FileInputStream("/opt/raisin/config/email.properties")) {
			props.load(inputStream);

			Session session = Session.getInstance(props, new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(props.getProperty("email.user"), props.getProperty("email.pass"));
				}
			});

			try {
				session.getTransport(props.getProperty("protocol"));
				
				// Construct the message
				String from = props.getProperty("from.email");
				Message msg = new MimeMessage(session);
			    msg.setFrom(new InternetAddress(from));

			    String[] emails = null;
			    if ( "error".equals(type) ) {
			    	emails = props.getProperty("error.to.email").split(";");
			    } else {
			    	emails = props.getProperty("to.email").split(";");
			    }

			    InternetAddress[] emailsIA = new InternetAddress[emails.length];
			    for (int i = 0; i < emails.length; i++) {
			    	emailsIA[i] = new InternetAddress(emails[i]);
			    }
			    
			    msg.setRecipients(Message.RecipientType.TO, emailsIA);
			    msg.setSubject(subject);
			    msg.setText(bodyMsg);

			    // Send the message.
			    Transport.send(msg);
			} catch (MessagingException e) {
				LOGGER.error("Failed to send email", e);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads the content of @param pathInput of type @param fileType.
	 * @param pathInput
	 * @param fileType
	 */
	private void readFileGeneric(String pathInput, String fileType) {

		MongoConnection mongo = MongoConnection.getInstance();
		RaisinImpl raisinImpl = new RaisinImpl();
		hibernateUtils = HibernateUtils.getInstance();
		File folder = new File(pathInput);
		File[] listOfFiles = folder.listFiles();
		LOGGER.debug("Creator process " + fileType + " started.");
		if (listOfFiles != null) {

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					LOGGER.info(i + " = " + listOfFiles[i].getName());

					if (fileType.equals("PRA")){
						sendEmail("Novo pedido RAISIN",
								"Recebemos um pedido de renovação com seguinte id:\n\n" + listOfFiles[i].getName(), "PRA");
					LOGGER.info("Enviado email com pedido de renovação.");

						try {
							FileUtils.moveDirectory(new File(pathInput + "/" + listOfFiles[i].getName()),
									new File(pathInput + "/OLD/" + listOfFiles[i].getName()));
						} catch (IOException e) {
							LOGGER.error("Failed to move file to '/OLD/'", e);
						}
					}

				} else {
					if (!"OLD".equals(listOfFiles[i].getName())) {
						String xml = readFile(
								listOfFiles[i].listFiles((dir, name) -> name.toLowerCase().contains(fileType.toLowerCase()))[0]);
						ObjectId objectId = processFile(xml, fileType);

						org.bson.Document doc = mongo.get("_id", objectId);
						int reqId = doc.getInteger("request_id");
						LOGGER.info("Updated reqId = " + reqId);
						Edge edge = raisinImpl.getNextEdge( fileType + "file", hibernateUtils, reqId);
						raisinImpl.evoluteRequest(reqId, edge.getStageByStageTo(), 5);

						try {
							FileUtils.moveDirectory(new File(pathInput + "/" + listOfFiles[i].getName()),
									new File(pathInput + "/OLD/" + listOfFiles[i].getName()));
						} catch (IOException e) {
							LOGGER.error("Failed to move file to '/OLD/'", e);
						}

					}
				}
			}
		}
		LOGGER.debug("Creator process " + fileType + " ended.");
		
	}

	private void saveFilesOnMongo(File[] listFiles, ObjectId id) {
		MongoConnection mongo = MongoConnection.getInstance();
		InputStream in;
		String name;
		for (File file : listFiles) {
			LOGGER.info("name = " + file.getName());

			try {
				in = new FileInputStream(file);
				name = file.getName();
				ObjectId fileId = mongo.saveInputStreamFile(in, name, id);
				mongo.updateDoc("_id", id, "attachments." + name.substring(name.length() - 7, name.length() - 4),
						fileId);
			} catch (FileNotFoundException e) {
				LOGGER.error("Failed to save files on Mongo", e);
			}
		}
	}

	private String readFile(File file) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String line;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			return sb.toString();
		} catch (IOException e) {
			LOGGER.error("BufferedReader failed to read the file", e);
		}
		return null;
	}

	private boolean isNewFile(String fileName) {
		try {
			MongoConnection mongo = MongoConnection.getInstance();
			if (mongo.get("term_deposit_id", fileName) == null) {
				return true;
			} 
		} catch (Exception e) {
			LOGGER.error("Failed to check if it's a new file.", e);
			return false;
		}
		return false;
	}

	private ObjectId processFile(String xml) {
		MongoConnection mongo = MongoConnection.getInstance();
		LOGGER.debug("Mongo connection started.");
		try {
			HashMap<String, String> map = processXML(xml);
			String termDepositId = map.get("term_deposit_id");

			if (map.containsKey("record_type_cd") && "PYI".equals(map.get("record_type_cd"))) {
				String payInDt = map.get("payin_dt").replaceAll("-", "");
				Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
				Map<String, String> newMap = new HashMap<>();
				while (iterator.hasNext()) {
					Map.Entry<String, String> entry = iterator.next();
					iterator.remove();
					newMap.put("PYI." + payInDt + "." + entry.getKey(), entry.getValue());
				}
				map.putAll(newMap);
			}
			JSONObject jsonObject = new JSONObject(map);

			LOGGER.info("json \n" + jsonObject + "\n..........");
			org.bson.Document doc = org.bson.Document.parse(jsonObject.toString());

			if (mongo.get("term_deposit_id", termDepositId) != null) {
				
				for (@SuppressWarnings("rawtypes") Entry entry : doc.entrySet()) {
					mongo.updateDoc("term_deposit_id", termDepositId, String.valueOf(entry.getKey()),
						String.valueOf(entry.getValue()));
					doc = mongo.get("term_deposit_id", termDepositId);
				}
				
			} else {
				mongo.insertOne(doc);

				@SuppressWarnings("unchecked")
				List<Request> requestList = hibernateUtils.getEntityManager()
						.createQuery("SELECT r FROM Request r WHERE r.context = :context ")
						.setParameter("context", doc.getObjectId("_id").toString()).getResultList();
				if (requestList == null || requestList.size() == 0) {
					createRequest(doc.getObjectId("_id").toString());
				}
			}
			mongo.updateDoc("term_deposit_id", termDepositId, "first_names", RaisinImpl.normalize(doc.getString("first_names")));
			mongo.updateDoc("term_deposit_id", termDepositId, "last_name", RaisinImpl.normalize(doc.getString("last_name")));
			LOGGER.debug("Process file on Mongo finished.");

			return doc.getObjectId("_id");
		} catch (Exception e) {
			LOGGER.error("Failed to process the file on Mongo.", e);
		}
		return null;

	}
	
	private ObjectId processFile(String xml, String fileType) {
		MongoConnection mongo = MongoConnection.getInstance();
		LOGGER.debug("Mongo connection started.");
		try {
			HashMap<String, String> map = processXML(xml);
			String termDepositId = map.get("term_deposit_id");
			if ("PRA".equals(fileType)) {
				termDepositId = map.get("maturing_term_deposit_id");
			}

			if (map.containsKey("record_type_cd") && fileType.equals(map.get("record_type_cd"))) {
				String receptionDate = map.get("receptionDate").replaceAll("-", "");
				Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
				Map<String, String> newMap = new HashMap<>();
				while (iterator.hasNext()) {
					Map.Entry<String, String> entry = iterator.next();
					iterator.remove();
					newMap.put(fileType + "." + receptionDate + "." + entry.getKey(), entry.getValue());
				}
				map.putAll(newMap);
			}
			JSONObject jsonObject = new JSONObject(map);

			LOGGER.info("json \n" + jsonObject + "\n..........");
			org.bson.Document doc = org.bson.Document.parse(jsonObject.toString());
			
			if (mongo.get("term_deposit_id", termDepositId) != null) {
				
				for (@SuppressWarnings("rawtypes") Entry entry : doc.entrySet()) {
					mongo.updateDoc("term_deposit_id", termDepositId, String.valueOf(entry.getKey()),
						String.valueOf(entry.getValue()));
					doc = mongo.get("term_deposit_id", termDepositId);
				}
				
			} else {
				mongo.insertOne(doc);

				@SuppressWarnings("unchecked")
				List<Request> requestList = hibernateUtils.getEntityManager()
						.createQuery("SELECT r FROM Request r WHERE r.context = :context ")
						.setParameter("context", doc.getObjectId("_id").toString()).getResultList();
				if (requestList == null || requestList.size() == 0) {
					createRequest(doc.getObjectId("_id").toString());
				}
			}
			LOGGER.debug("Process file on Mongo finished.");
			return doc.getObjectId("_id");
		} catch (Exception e) {
			LOGGER.error("Failed to process the file on Mongo.", e);
		}
		return null;

	}

	private void createRequest(String context) {
		MongoConnection mongo = MongoConnection.getInstance();
		LOGGER.debug("Mongo connection started.");
		try {

			Status status = hibernateUtils.getEntityManager().find(Status.class, 1);

			Request request = new Request();
			request.setContext(context);
			request.setOwnerId(5);
			request.setStatus(status);
			request.setReference("UNDEFINED");
			request.setCreationDate(new Date());

			hibernateUtils.getEntityManager().getTransaction().begin();
			hibernateUtils.getEntityManager().persist(request);
			hibernateUtils.getEntityManager().flush();

			Workflow workflow = hibernateUtils.getEntityManager().find(Workflow.class, 3);
			Stage stage = hibernateUtils.getEntityManager().find(Stage.class, 62);
			Position position = new Position();
			position.setId(new PositionId(request.getId(), stage.getId(), workflow.getId()));
			position.setRequest(request);
			position.setStage(stage);
			position.setWorkflow(workflow);
			position.setChangeDate(new Date());

			hibernateUtils.getEntityManager().persist(position);
			hibernateUtils.getEntityManager().flush();

			History history = new History(
					new HistoryId(request.getId(), stage.getId(), workflow.getId(), 5, "N/A", new Date()), request,
					stage, workflow);
			hibernateUtils.getEntityManager().persist(history);
			hibernateUtils.getEntityManager().getTransaction().commit();

			mongo.updateDoc("_id", new ObjectId(context), "request_id", request.getId());
			LOGGER.debug("Create file on Mongo finished.");
		} catch (Exception e) {
			LOGGER.error("Failed to creat a request.", e);
		}
	}
}
