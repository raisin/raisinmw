//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package pt.bai.dbmanager.utils;

import com.ibm.as400.access.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

public class SystemUtils {
    private static final Logger LOGGER = Logger.getLogger(SystemUtils.class);

    public SystemUtils() {
    }

    public static boolean isSystemAvailableAllTime(String hostname, String username, String password, String SOFTWARE, String LIB) {
        AS400 system = null;

        try {
            system = new AS400();
            system.setGuiAvailable(false);
            system.setSystemName(hostname);
            system.setUserId(username);
            system.setPassword(password);
            system.connectService(0);
            CommandCall command = new CommandCall(system);
            String callString = "CALL PGM(BMEXT) PARM('" + LIB + "' '" + SOFTWARE + "' 'N') ";
            command.run(callString);
            boolean resultBANKA = getStatus(system, LIB);
            LOGGER.debug("BANKA disponivel -> " + resultBANKA);
            if (resultBANKA) {
                boolean resultTape = !isSavingTape(system);
                LOGGER.debug("Segurança a executar -> " + !resultTape);
                boolean var11 = resultTape;
                return var11;
            }

            return false;
        } catch (AS400SecurityException | IOException | ErrorCompletingRequestException | InterruptedException | ObjectDoesNotExistException | PropertyVetoException var14) {
            LOGGER.error(var14, var14);
        } finally {
            system.disconnectAllServices();
        }

        return false;
    }

    public static boolean getSystemStatus(String hostname, String username, String password, String SOFTWARE, String LIB) {
        try {
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            dateFormat.format(date);
            if (dateFormat.parse(dateFormat.format(date)).after(dateFormat.parse("17:15")) && dateFormat.parse(dateFormat.format(date)).before(dateFormat.parse("20:45"))) {
                return false;
            } else if (dateFormat.parse(dateFormat.format(date)).after(dateFormat.parse("16:30"))) {
                AS400 system = null;

                try {
                    system = new AS400();
                    system.setGuiAvailable(false);
                    system.setSystemName(hostname);
                    system.setUserId(username);
                    system.setPassword(password);
                    system.connectService(0);
                    CommandCall command = new CommandCall(system);
                    String callString = "CALL PGM(BMEXT) PARM('" + LIB + "' '" + SOFTWARE + "' 'N') ";
                    command.run(callString);
                    boolean var11 = getStatus(system, LIB);
                    return var11;
                } catch (AS400SecurityException | IOException | ErrorCompletingRequestException | InterruptedException | ObjectDoesNotExistException | PropertyVetoException var15) {
                    var15.printStackTrace();
                } finally {
                    system.disconnectAllServices();
                }

                return false;
            } else {
                return true;
            }
        } catch (ParseException var17) {
            Logger.getLogger(SystemUtils.class.getName()).log(Level.ERROR, var17);
            return false;
        }
    }

    private static boolean getStatus(AS400 system, String lib) throws AS400SecurityException, ErrorCompletingRequestException, IOException, InterruptedException, ObjectDoesNotExistException, PropertyVetoException {
        AS400PackedDecimal param1 = new AS400PackedDecimal(1, 0);
        AS400PackedDecimal param2 = new AS400PackedDecimal(1, 0);
        ProgramParameter[] parmList = new ProgramParameter[]{new ProgramParameter(param1.toBytes(new BigDecimal(0)), param1.getByteLength()), new ProgramParameter(param2.toBytes(new BigDecimal(0)), param2.getByteLength())};
        String pgmString = "/QSYS.LIB/PROUSR" + lib + ".LIB/ISONLINE.PGM";
        ProgramCall pgm = new ProgramCall(system, pgmString, parmList);
        if (pgm.run()) {
            BigDecimal banka = (BigDecimal)(new AS400PackedDecimal(1, 0)).toObject(parmList[0].getOutputData());
            BigDecimal financa = (BigDecimal)(new AS400PackedDecimal(1, 0)).toObject(parmList[1].getOutputData());
            return ("" + banka + financa).equals("00");
        } else {
            return false;
        }
    }

    private static boolean isSavingTapeByJobName(JobList jobList, String jobname) throws PropertyVetoException, AS400SecurityException, ErrorCompletingRequestException, InterruptedException, IOException, ObjectDoesNotExistException {
        jobList.addJobSelectionCriteria(5, Boolean.TRUE);
        jobList.addJobSelectionCriteria(6, Boolean.FALSE);
        jobList.addJobSelectionCriteria(7, Boolean.FALSE);
        jobList.addJobSelectionCriteria(1, jobname);
        Enumeration jobs = jobList.getJobs();
        return jobs.hasMoreElements();
    }

    public static boolean isSavingTape(AS400 system) {
        try {
            JobList jobList = new JobList(system);
            return isSavingTapeByJobName(jobList, "SAVEPORTO") || isSavingTapeByJobName(jobList, "QEZBKTMMON") || isSavingTapeByJobName(jobList, "QEZBKTMTUE") || isSavingTapeByJobName(jobList, "QEZBKTMWED") || isSavingTapeByJobName(jobList, "QEZBKTMTHU") || isSavingTapeByJobName(jobList, "QEZBKTMFRI");
        } catch (AS400SecurityException | IOException | ErrorCompletingRequestException | InterruptedException | ObjectDoesNotExistException | PropertyVetoException var2) {
            Logger.getLogger(SystemUtils.class.getName()).log(Level.ERROR, var2);
            return false;
        }
    }
}
