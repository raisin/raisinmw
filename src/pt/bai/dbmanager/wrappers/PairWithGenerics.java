package pt.bai.dbmanager.wrappers;

import java.util.Objects;

public class PairWithGenerics<K, V> {

    private final K key;
    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    private final V value;

    public PairWithGenerics(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public boolean equals(Object o) {
        return o instanceof PairWithGenerics && Objects.equals(key, ((PairWithGenerics<?,?>)o).key) && Objects.equals(value, ((PairWithGenerics<?,?>)o).value);
    }

    public int hashCode() {
        return 31 * Objects.hashCode(key) + Objects.hashCode(value);
    }

    public String toString() {
        return key + "=" + value;
    }
}
