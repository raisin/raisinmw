/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager.wrappers;

/**
 *
 * @author loliveira
 */
public class ClientTotalInfo {

    private String numero;
    private String nome;
    private String morada1;
    private String morada2;
    private String localidade;

    public ClientTotalInfo() {
    }

    public ClientTotalInfo(String numero, String nome, String morada1, String morada2, String localidade) {
        this.numero = numero;
        this.nome = nome;
        this.morada1 = morada1;
        this.morada2 = morada2;
        this.localidade = localidade;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada1() {
        return morada1;
    }

    public void setMorada1(String morada1) {
        this.morada1 = morada1;
    }

    public String getMorada2() {
        return morada2;
    }

    public void setMorada2(String morada2) {
        this.morada2 = morada2;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }




}