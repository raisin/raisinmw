/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager.wrappers;

import javax.jws.WebMethod;
import javax.xml.bind.annotation.XmlElement;
import java.util.LinkedList;

/**
 *
 * @author loliveira
 */
public class CurrencyCorrespondents {

    private LinkedList<String> lista;

    public CurrencyCorrespondents() {
    }

    public CurrencyCorrespondents(LinkedList<String> lista) {
        this.lista = lista;
    }

    @WebMethod
    @XmlElement(name = "lista")
    public LinkedList<String> getLista() {
        return lista;
    }

    public void setLista(LinkedList<String> lista) {
        this.lista = lista;
    }

}
