/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager.wrappers;

import java.math.BigDecimal;

/**
 *
 * @author loliveira
 */
public class ResultContacliente {

    private String cliente;
    private String nome;
    private String morada;
    private String conta;
    private String NIB;
    private String IBAN;
    private String moeda;
    private String balcao;
    private BigDecimal saldo;
    private String IB;
    private String pais;

    public ResultContacliente() {
    }

    /**
     *
     * @param cliente
     * @param nome
     * @param morada
     * @param conta
     * @param NIB
     * @param moeda
     * @param saldo
     */
    public ResultContacliente(String cliente, String nome, String morada, String conta, String NIB, String moeda, BigDecimal saldo) {
        this.cliente = cliente;
        this.nome = nome;
        this.morada = morada;
        this.conta = conta;
        this.NIB = NIB;
        this.moeda = moeda;
        this.saldo = saldo;
    }

    public ResultContacliente(String cliente, String nome, String morada, String conta, String NIB, String moeda, BigDecimal saldo, String balcao) {
        this.cliente = cliente;
        this.nome = nome;
        this.morada = morada;
        this.conta = conta;
        this.NIB = NIB;
        this.moeda = moeda;
        this.balcao = balcao;
        this.saldo = saldo;
    }

    public ResultContacliente(String cliente, String nome, String morada, String conta, String NIB, String moeda, BigDecimal saldo, String balcao, String IB) {
        this.cliente = cliente;
        this.nome = nome;
        this.morada = morada;
        this.conta = conta;
        this.NIB = NIB;
        this.moeda = moeda;
        this.balcao = balcao;
        this.saldo = saldo;
        this.IB = IB;
    }

    public ResultContacliente(String cliente, String nome, String morada, String conta, String NIB, String moeda, BigDecimal saldo, String balcao, String IB, String pais) {
        this.cliente = cliente;
        this.nome = nome;
        this.morada = morada;
        this.conta = conta;
        this.NIB = NIB;
        this.moeda = moeda;
        this.balcao = balcao;
        this.saldo = saldo;
        this.IB = IB;
        this.pais = pais;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getNIB() {
        return NIB;
    }

    public void setNIB(String NIB) {
        this.NIB = NIB;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public String getBalcao() {
        return balcao;
    }

    public void setBalcao(String balcao) {
        this.balcao = balcao;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getIBAN() {
        return "PT50" + getNIB();
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getIB() {
        return IB;
    }

    public void setIB(String IB) {
        this.IB = IB;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

}