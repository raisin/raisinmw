/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager.wrappers;

/**
 *
 * @author loliveira
 */
public class Pair {

    private String A;
    private String B;

    public Pair() {
    }

    public Pair(String A, String B) {
        this.A = A;
        this.B = B;
    }

    public String getA() {
        return A;
    }

    public void setA(String A) {
        this.A = A;
    }

    public String getB() {
        return B;
    }

    public void setB(String B) {
        this.B = B;
    }

}