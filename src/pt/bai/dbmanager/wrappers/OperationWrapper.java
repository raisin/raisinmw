/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager.wrappers;

/**
 *
 * @author loliveira
 */
public class OperationWrapper {

    private String mrccb;
    private String mrvlr;
    private String mrmoed;
    private String mrdtsis;
    private String mrnopr;
    private String MRDMOV;
    private String MVDATV;

    public OperationWrapper() {
    }

    public OperationWrapper(String mrccb, String mrvlr, String mrmoed, String mrdtsis, String mrnopr, String MRDMOV) {
        this.mrccb = mrccb;
        this.mrvlr = mrvlr;
        this.mrmoed = mrmoed;
        this.mrdtsis = mrdtsis;
        this.mrnopr = mrnopr;
        this.MRDMOV = MRDMOV;
    }

    public OperationWrapper(String mrccb, String mrvlr, String mrmoed, String mrdtsis, String mrnopr, String MRDMOV, String MVDATV) {
        this.mrccb = mrccb;
        this.mrvlr = mrvlr;
        this.mrmoed = mrmoed;
        this.mrdtsis = mrdtsis;
        this.mrnopr = mrnopr;
        this.MRDMOV = MRDMOV;
        this.MVDATV = MVDATV;
    }

    public String getMrccb() {
        return mrccb;
    }

    public void setMrccb(String mrccb) {
        this.mrccb = mrccb;
    }

    public String getMrvlr() {
        return mrvlr;
    }

    public void setMrvlr(String mrvlr) {
        this.mrvlr = mrvlr;
    }

    public String getMrmoed() {
        return mrmoed;
    }

    public void setMrmoed(String mrmoed) {
        this.mrmoed = mrmoed;
    }

    public String getMrdtsis() {
        return mrdtsis;
    }

    public void setMrdtsis(String mrdtsis) {
        this.mrdtsis = mrdtsis;
    }

    public String getMrnopr() {
        return mrnopr;
    }

    public void setMrnopr(String mrnopr) {
        this.mrnopr = mrnopr;
    }

    public String getMRDMOV() {
        return MRDMOV;
    }

    public void setMRDMOV(String MRDMOV) {
        this.MRDMOV = MRDMOV;
    }

    public String getMVDATV() {
        return MVDATV;
    }

    public void setMVDATV(String MVDATV) {
        this.MVDATV = MVDATV;
    }



}