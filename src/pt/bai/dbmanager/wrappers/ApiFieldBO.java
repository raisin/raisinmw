package pt.bai.dbmanager.wrappers;

public class ApiFieldBO {

    private String colName;

    private String desc;

    private String colValue;

    private String mandatoryOptional;

    public ApiFieldBO(String colName, String desc, String mandatoryOptional) {
        this.colName = colName;
        this.desc = desc;
        this.mandatoryOptional = mandatoryOptional;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getColValue() {
        return colValue;
    }

    public void setColValue(String colValue) {
        this.colValue = colValue;
    }

    public String getMandatoryOptional() {
        return mandatoryOptional;
    }

    public void setMandatoryOptional(String mandatoryOptional) {
        this.mandatoryOptional = mandatoryOptional;
    }

}