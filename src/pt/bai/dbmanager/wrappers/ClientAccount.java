/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager.wrappers;

import javax.jws.WebMethod;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author loliveira
 */
public class ClientAccount {

    private String clientNumber;
    private String clientName;
    private String accountNumber;
    private String accountIBAN;
    private String currency;
    private String balance;

    public ClientAccount() {
    }

    public ClientAccount(String clientNumber, String clientName, String accountNumber, String accountIBAN, String moeda) {
        this.clientNumber = clientNumber;
        this.clientName = clientName;
        this.accountNumber = accountNumber;
        this.accountIBAN = accountIBAN;
        this.currency = moeda;
        this.balance = "0.00";
    }

    public ClientAccount(String clientNumber, String clientName, String accountNumber, String accountIBAN, String moeda, String balance) {
        this.clientNumber = clientNumber;
        this.clientName = clientName;
        this.accountNumber = accountNumber;
        this.accountIBAN = accountIBAN;
        this.currency = moeda;
        this.balance = balance;
    }

    @WebMethod
    @XmlElement(name = "numeroCliente")
    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    @WebMethod
    @XmlElement(name = "nomeCliente")
    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @WebMethod
    @XmlElement(name = "numeroConta")
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @WebMethod
    @XmlElement(name = "IBANConta")
    public String getAccountIBAN() {
        return accountIBAN;
    }

    public void setAccountIBAN(String accountIBAN) {
        this.accountIBAN = accountIBAN;
    }

    @WebMethod
    @XmlElement(name = "moeda")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @WebMethod
    @XmlElement(name = "saldo")
    public String getBalance() {
        NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "PT"));
        DecimalFormat df = (DecimalFormat) nf;

        return df.format(new BigDecimal(balance));
    }

    public BigDecimal getBigDecimalBalance(){
        return new BigDecimal(balance);
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
//
//    @WebMethod
//    @XmlElement(name = "Saldo")
//    public String getSaldo() {
//        NumberFormat nf = NumberFormat.getNumberInstance(new Locale("pt", "PT"));
//        DecimalFormat df = (DecimalFormat) nf;
//
//        return df.format(new BigDecimal(balance));
//    }

}