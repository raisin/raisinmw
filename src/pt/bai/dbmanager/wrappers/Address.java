/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager.wrappers;

/**
 *
 * @author loliveira
 */
public class Address {

    private String type;
    private String address;

    public Address(String type, String address) {
        this.type = type;
        this.address = address;
    }

    public String getAddress() {
        return address.toUpperCase();
    }

    public void setMorada(String address) {
        this.address = address;
    }

    public String getType() {
        return type.toUpperCase();
    }

    public void setType(String type) {
        this.type = type;
    }

}