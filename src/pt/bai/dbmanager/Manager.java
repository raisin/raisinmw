//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package pt.bai.dbmanager;

import com.ibm.as400.access.AS400JDBCDataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import pt.bai.dbmanager.utils.SystemUtils;
import pt.bai.dbmanager.wrappers.*;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Manager {
    private static Logger log = Logger.getLogger(Manager.class.getName());
    private Connection conBANKA;
    private AS400JDBCDataSource bankads;
    private HashMap<Query, PreparedStatement> statements;
    private String SoftwareVersion;
    private String LibVersion;
    private Context context;
    private ManagerParameters managerParameters;
    private ConcurrentHashMap<String, Pair> getBankCountryFromCCBResults;
    private ConcurrentHashMap<String, String> isNOSTROVOSTROResults;
    private ConcurrentHashMap<String, String> isVOSTRONoCurrencyResults;
    private ConcurrentHashMap<String, ResultContacliente> getAccountNIBResults;
    private ConcurrentHashMap<String, ResultContacliente> getDOAccountInfoResults;
    private ConcurrentHashMap<String, String> getBranchAndAccountFromNIBResults;
    private ConcurrentHashMap<String, ResultContacliente> getBranchAndAccountAndClienteAndCurrencyFromIBANResults;
    private ConcurrentHashMap<String, String> getBranchAndAccountFromAccountResults;
    private ConcurrentHashMap<String, String> getBICfromVOSTROResults;
    private ConcurrentHashMap<String, LinkedList<Pair>> getListVOSTROFromBICAndCurrencyResults;
    private ConcurrentHashMap<String, String> getNOSTROFromBICAndCurrencyResults;
    private ConcurrentHashMap<String, LinkedList<Pair>> getAllNOSTROFromBICAndCurrencyResults;
    private ConcurrentHashMap<String, String> getNameFromCCBResults;

    public Manager(String SoftwareVersion, String LibVersion, ManagerParameters managerParameters) {
        this.SoftwareVersion = SoftwareVersion;
        this.LibVersion = LibVersion;
        this.managerParameters = managerParameters;
        this.getBankCountryFromCCBResults = new ConcurrentHashMap();
        this.isNOSTROVOSTROResults = new ConcurrentHashMap();
        this.isVOSTRONoCurrencyResults = new ConcurrentHashMap();
        this.getAccountNIBResults = new ConcurrentHashMap();
        this.getDOAccountInfoResults = new ConcurrentHashMap();
        this.getBranchAndAccountFromNIBResults = new ConcurrentHashMap();
        this.getBranchAndAccountAndClienteAndCurrencyFromIBANResults = new ConcurrentHashMap();
        this.getBranchAndAccountFromAccountResults = new ConcurrentHashMap();
        this.getBICfromVOSTROResults = new ConcurrentHashMap();
        this.getListVOSTROFromBICAndCurrencyResults = new ConcurrentHashMap();
        this.getNOSTROFromBICAndCurrencyResults = new ConcurrentHashMap();
        this.getAllNOSTROFromBICAndCurrencyResults = new ConcurrentHashMap();
        this.getNameFromCCBResults = new ConcurrentHashMap();
        this.statements = new HashMap(Query.values().length);
    }

    public Manager(String SoftwareVersion, String LibVersion) {
        this.SoftwareVersion = SoftwareVersion;
        this.LibVersion = LibVersion;
        this.managerParameters = new ManagerParameters();
        this.getBankCountryFromCCBResults = new ConcurrentHashMap();
        this.isNOSTROVOSTROResults = new ConcurrentHashMap();
        this.isVOSTRONoCurrencyResults = new ConcurrentHashMap();
        this.getAccountNIBResults = new ConcurrentHashMap();
        this.getDOAccountInfoResults = new ConcurrentHashMap();
        this.getBranchAndAccountFromNIBResults = new ConcurrentHashMap();
        this.getBranchAndAccountAndClienteAndCurrencyFromIBANResults = new ConcurrentHashMap();
        this.getBranchAndAccountFromAccountResults = new ConcurrentHashMap();
        this.getBICfromVOSTROResults = new ConcurrentHashMap();
        this.getListVOSTROFromBICAndCurrencyResults = new ConcurrentHashMap();
        this.getNOSTROFromBICAndCurrencyResults = new ConcurrentHashMap();
        this.getAllNOSTROFromBICAndCurrencyResults = new ConcurrentHashMap();
        this.getNameFromCCBResults = new ConcurrentHashMap();
        this.statements = new HashMap(Query.values().length);
    }

    public void setManagerParameters(ManagerParameters managerParameters) {
        this.managerParameters = managerParameters;
    }

    public ManagerParameters getManagerParameters() {
        return this.managerParameters;
    }

    public void release() {
        try {
            this.conBANKA.rollback();
        } catch (SQLException var2) {
            log.error(var2.getMessage());
        }

    }

    public boolean startUp(String user, String password, String hostname) {
        this.bankads = null;
        if (SystemUtils.isSystemAvailableAllTime(hostname, user, password, this.SoftwareVersion, this.LibVersion)) {
            this.bankads = new AS400JDBCDataSource(hostname);
            this.bankads.setPrompt(false);
            this.bankads.setUser(user.toUpperCase());
            this.bankads.setPassword(password.toUpperCase());
            this.bankads.setLibraries("GBANCA" + this.LibVersion);
            this.bankads.setLibraries("PROFG" + this.LibVersion);
            this.bankads.setCursorHold(false);

            try {
                this.conBANKA = this.bankads.getConnection();
                this.conBANKA.setTransactionIsolation(0);
                this.conBANKA.setReadOnly(true);
                this.conBANKA.setHoldability(2);
            } catch (SQLException var5) {
                log.error(var5.getMessage());
                return false;
            }

            this.context = new Context(this.LibVersion);
            return true;
        } else {
            return false;
        }
    }

    public boolean startUpOffline(String user, String password, String hostname) {
        this.bankads = null;
        this.bankads = new AS400JDBCDataSource(hostname);
        this.bankads.setPrompt(false);
        this.bankads.setUser(user.toUpperCase());
        this.bankads.setPassword(password.toUpperCase());
        this.bankads.setLibraries("GBANCA" + this.LibVersion);
        this.bankads.setLibraries("PROFG" + this.LibVersion);
        this.bankads.setCursorHold(false);

        try {
            this.conBANKA = this.bankads.getConnection();
            this.conBANKA.setTransactionIsolation(0);
            this.conBANKA.setReadOnly(true);
            this.conBANKA.setHoldability(2);
        } catch (SQLException var5) {
            log.error(var5.getMessage());
            return false;
        }

        this.context = new Context(this.LibVersion);
        return true;
    }

    public PreparedStatement getStatement(Query q) {
        PreparedStatement s = (PreparedStatement)this.statements.get(q);

        try {
            s.clearParameters();
        } catch (SQLException var4) {
            log.error(var4.getMessage());
        }

        return s;
    }

    public boolean isUp() {
        try {
            return this.conBANKA.isClosed();
        } catch (SQLException var2) {
            log.error(var2.getMessage());
            return false;
        }
    }

    public void closeAll() {
        try {
            if (!this.conBANKA.isClosed()) {
                this.conBANKA.close();
            }
        } catch (SQLException var2) {
            log.error(var2.getMessage());
        }

    }

    public HashMap<String, String> getOperationDescription() {
        ResultSet set = null;
        PreparedStatement stat = null;
        HashMap result = new HashMap();

        HashMap var6;
        try {
            stat = this.conBANKA.prepareStatement(this.context.getOperationDescription());
            stat.clearParameters();
            set = stat.executeQuery();

            while(set.next()) {
                result.put(set.getString(1), set.getString(2));
            }

            return result;
        } catch (Exception var14) {
            log.error(var14.getMessage());
            var6 = result;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var13) {
                    log.error(var13.getMessage());
                }
            }

        }

        return var6;
    }

    public Pair getBankCountryFromCCB(String ccb) {
        if (this.getBankCountryFromCCBResults.containsKey(ccb)) {
            return (Pair)this.getBankCountryFromCCBResults.get(ccb);
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;
            Pair result;
            if (ccb.trim().equals("548914")) {
                result = new Pair("EBAS", "BE");
                this.getBankCountryFromCCBResults.put(ccb, result);
                return result;
            } else {
                try {
                    stat = this.conBANKA.prepareStatement(this.context.getBankCountryFromCCB());
                    stat.clearParameters();
                    stat.setString(1, ccb + "%");
                    set = stat.executeQuery();
                    if (set.next()) {
                        result = new Pair(set.getString("nvtccb").substring(4, 8), set.getString("nvtccb").substring(8, 10));
                        this.getBankCountryFromCCBResults.put(ccb, result);
                        Pair var6 = result;
                        return var6;
                    }

                    return null;
                } catch (Exception var15) {
                    log.error(var15.getMessage());
                } finally {
                    if (set != null) {
                        try {
                            set.close();
                            stat.clearParameters();
                            stat.close();
                        } catch (SQLException var14) {
                            log.error(var14.getMessage());
                        }
                    }

                }

                return null;
            }
        }
    }

    public String isNOSTROVOSTRO(String BIC, String currency, boolean offline) {
        if (this.isNOSTROVOSTROResults.containsKey(BIC + " || " + currency)) {
            return (String)this.isNOSTROVOSTROResults.get(BIC + " || " + currency);
        } else if (BIC.equals("BGALPTPL")) {
            return this.managerParameters.getTargetAccount();
        } else if (BIC.trim().equals("STEP1")) {
            return "548914";
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            String var11;
            try {
                Pair par = this.getBankAndCountryFromSWIFT(BIC, offline);
                if (par == null) {
                    this.isNOSTROVOSTROResults.put(BIC + " || " + currency, this.managerParameters.getSemResultados());
                    var11 = this.managerParameters.getSemResultados();
                    return var11;
                }

                String ban = par.getA();
                String pai = par.getB();
                if (ban.equals("SCBL") && pai.equals("US")) {
                    ban = "AEIB";
                }

                stat = this.conBANKA.prepareStatement(this.context.getNOSTRO());
                stat.clearParameters();
                stat.setString(1, "2010" + ban + pai + currency + "%");
                set = stat.executeQuery();
                String result;
                if (set.next() && set.getString("NVCCB").length() > 3) {
                    result = set.getString("NVCCB");
                    this.isNOSTROVOSTROResults.put(BIC + " || " + currency, result);
                    var11 = result;
                    return var11;
                }

                set.close();
                stat.close();
                if (offline) {
                    stat = this.conBANKA.prepareStatement(this.context.getVOSTROcurrency());
                } else {
                    stat = this.conBANKA.prepareStatement(this.context.getVOSTROcurrencyOffline());
                }

                stat.clearParameters();
                stat.setString(1, ban);
                stat.setString(2, pai);
                stat.setString(3, currency);
                set = stat.executeQuery();
                if (!set.next() || set.getString("CICPOS").trim().equals("")) {
                    return this.managerParameters.getSemResultados();
                }

                result = set.getString("CICPOS");
                this.isNOSTROVOSTROResults.put(BIC + " || " + currency, result);
                var11 = result;
                return var11;
            } catch (Exception var22) {
                log.error(var22.getMessage());
                var11 = this.managerParameters.getErro();
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var21) {
                        log.error(var21.getMessage());
                    }
                }

            }

            return var11;
        }
    }

    public String isVOSTRONoCurrency(String BIC, boolean offline) {
        if (this.isVOSTRONoCurrencyResults.containsKey(BIC)) {
            return (String)this.isVOSTRONoCurrencyResults.get(BIC);
        } else if (BIC.equals("BGALPTPL")) {
            return this.managerParameters.getTargetAccount();
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            String var10;
            try {
                Pair par = this.getBankAndCountryFromSWIFT(BIC, offline);
                if (par == null) {
                    this.isVOSTRONoCurrencyResults.put(BIC, this.managerParameters.getSemResultados());
                    var10 = this.managerParameters.getSemResultados();
                    return var10;
                }

                String ban = par.getA();
                String pai = par.getB();
                if (ban.equals("SCBL") && pai.equals("US")) {
                    ban = "AEIB";
                }

                if (offline) {
                    stat = this.conBANKA.prepareStatement(this.context.getVOSTROOffline());
                } else {
                    stat = this.conBANKA.prepareStatement(this.context.getVOSTRO());
                }

                stat.clearParameters();
                stat.setString(1, ban);
                stat.setString(2, pai);
                set = stat.executeQuery();
                if (set.next() && !set.getString("CICPOS").trim().equals("")) {
                    String result = set.getString("CICPOS");
                    this.isVOSTRONoCurrencyResults.put(BIC, result);
                    var10 = result;
                    return var10;
                }

                return this.managerParameters.getSemResultados();
            } catch (Exception var20) {
                log.error(var20.getMessage());
                var10 = this.managerParameters.getErro();
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var19) {
                        log.error(var19.getMessage());
                    }
                }

            }

            return var10;
        }
    }

    public ResultContacliente getAccountNIB(String NIB) {
        if (this.getAccountNIBResults.containsKey(NIB)) {
            return (ResultContacliente)this.getAccountNIBResults.get(NIB);
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            try {
                stat = this.conBANKA.prepareStatement(this.context.getAccountNIB());
                stat.clearParameters();
                stat.setString(1, NIB);
                set = stat.executeQuery();
                if (!set.next()) {
                    return null;
                }

                ResultContacliente result = new ResultContacliente(set.getString("CLIENTE"), set.getString("NOME"), set.getString("MORADA1").trim() + "\n" + set.getString("MORADA2").trim() + "\n" + set.getString("CODEX").trim(), set.getString("CONTA"), set.getString("NIB"), set.getString("MOEDA"), set.getBigDecimal("SALDO"), set.getString("BALCAO"), set.getString("IB"));
                this.getAccountNIBResults.put(NIB, result);
                ResultContacliente var6 = result;
                return var6;
            } catch (Exception var15) {
                log.error(var15.getMessage());
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var14) {
                        log.error(var14.getMessage());
                    }
                }

            }

            return null;
        }
    }

    public ResultContacliente getDOAccountInfo(String account) {
        if (this.getDOAccountInfoResults.containsKey(account)) {
            return (ResultContacliente)this.getDOAccountInfoResults.get(account);
        } else if ((new BigDecimal(account)).compareTo(new BigDecimal("99999999999999")) == 1) {
            return null;
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            try {
                stat = this.conBANKA.prepareStatement(this.context.getDOAccountInfo());
                stat.clearParameters();
                stat.setBigDecimal(1, new BigDecimal(account));
                set = stat.executeQuery();
                if (!set.next()) {
                    return null;
                }

                ResultContacliente result = new ResultContacliente(set.getString("CLIENTE"), set.getString("NOME"), set.getString("MORADA1").trim() + "\n" + set.getString("MORADA2").trim() + "\n" + set.getString("CODEX"), set.getString("CONTA"), set.getString("NIB"), set.getString("MOEDA"), set.getBigDecimal("SALDO"), set.getString("BALCAO"), set.getString("IB"));
                this.getDOAccountInfoResults.put(account, result);
                ResultContacliente var6 = result;
                return var6;
            } catch (Exception var15) {
                log.error(var15.getMessage());
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var14) {
                        log.error(var14.getMessage());
                    }
                }

            }

            return null;
        }
    }

    public String getBranchAndAccountFromNIB(String NIB) {
        if (this.getBranchAndAccountFromNIBResults.containsKey(NIB)) {
            return (String)this.getBranchAndAccountFromNIBResults.get(NIB);
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            try {
                stat = this.conBANKA.prepareStatement(this.context.getAccountNIB());
                stat.clearParameters();
                stat.setString(1, NIB);
                set = stat.executeQuery();
                if (!set.next()) {
                    return null;
                }

                String result = set.getString("CONTA") + "-" + set.getString("BALCAO") + "-" + set.getString("MOEDA");
                this.getBranchAndAccountFromNIBResults.put(NIB, result);
                String var6 = result;
                return var6;
            } catch (Exception var15) {
                log.error(var15.getMessage());
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var14) {
                        log.error(var14.getMessage());
                    }
                }

            }

            return null;
        }
    }

    public ResultContacliente getBranchAndAccountAndClienteAndCurrencyFromIBAN(String iban) {
        if (this.getBranchAndAccountAndClienteAndCurrencyFromIBANResults.containsKey(iban)) {
            return (ResultContacliente)this.getBranchAndAccountAndClienteAndCurrencyFromIBANResults.get(iban);
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            try {
                stat = this.conBANKA.prepareStatement(this.context.getAccountNIB());
                stat.clearParameters();
                stat.setString(1, iban);
                set = stat.executeQuery();
                if (set.next()) {
                    ResultContacliente result = new ResultContacliente(set.getString("CLIENTE"), set.getString("NOME"), set.getString("MORADA1").trim() + "\n" + set.getString("MORADA2").trim() + "\n" + set.getString("CODEX"), set.getString("CONTA"), set.getString("NIB"), set.getString("MOEDA"), set.getBigDecimal("SALDO"));
                    this.getBranchAndAccountAndClienteAndCurrencyFromIBANResults.put(iban, result);
                    ResultContacliente var6 = result;
                    return var6;
                }
            } catch (Exception var15) {
                log.error(var15.getMessage());
                return null;
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var14) {
                        log.error(var14.getMessage());
                    }
                }

            }

            return null;
        }
    }

    public String getBranchAndAccountFromAccount(String account) {
        if (this.getBranchAndAccountFromAccountResults.containsKey(account)) {
            return (String)this.getBranchAndAccountFromAccountResults.get(account);
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            try {
                stat = this.conBANKA.prepareStatement(this.context.getAccountNIB());
                stat.clearParameters();
                stat.setString(1, account);
                set = stat.executeQuery();
                if (!set.next()) {
                    return null;
                }

                String result = set.getString("CONTA") + "-" + set.getString("BALCAO");
                this.getBranchAndAccountFromAccountResults.put(account, result);
                String var6 = result;
                return var6;
            } catch (Exception var15) {
                log.error(var15.getMessage());
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var14) {
                        log.error(var14.getMessage());
                    }
                }

            }

            return null;
        }
    }

    public String getBICfromVOSTRO(String vostro) {
        if (this.getBICfromVOSTROResults.containsKey(vostro)) {
            return (String)this.getBICfromVOSTROResults.get(vostro);
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;

            String var6;
            try {
                set = null;
                stat = null;
                stat = this.conBANKA.prepareStatement(this.context.getBICfromVOSTRO());
                stat.clearParameters();
                stat.setString(1, "%" + vostro + "%");
                set = stat.executeQuery();
                if (!set.next()) {
                    return null;
                }

                String result = set.getString("DOSWIF").trim().substring(0, 8);
                this.getBICfromVOSTROResults.put(vostro, result);
                var6 = result;
            } catch (Exception var15) {
                log.error(var15.getMessage());
                return null;
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var14) {
                        log.error(var14.getMessage());
                    }
                }

            }

            return var6;
        }
    }

    public LinkedList<Pair> getListVOSTROFromBICAndCurrency(String BIC, String currency) {
        if (this.getListVOSTROFromBICAndCurrencyResults.containsKey("BIC || currency")) {
            return (LinkedList)this.getListVOSTROFromBICAndCurrencyResults.get("BIC || currency");
        } else {
            LinkedList<Pair> result = new LinkedList();
            ResultSet set = null;
            PreparedStatement stat = null;

            label164: {
                try {
                    Pair par = this.getBankAndCountryFromSWIFT(BIC, false);
                    if (par == null) {
                        return null;
                    }

                    String ban = par.getA();
                    String pai = par.getB();
                    set = null;
                    stat = null;
                    stat = this.conBANKA.prepareStatement(this.context.getVOSTRO());
                    stat.clearParameters();
                    stat.setString(1, ban);
                    stat.setString(2, pai);
                    set = stat.executeQuery();
                    LinkedList<Pair> temp = new LinkedList();
                    boolean OK = false;

                    while(set.next()) {
                        if (!set.getString("CICPOS").trim().isEmpty()) {
                            if (set.getString("CICDIV").equals(currency)) {
                                if (OK) {
                                    result.add(new Pair(set.getString("CICPOS"), set.getString("CICDIV")));
                                } else {
                                    result.add(new Pair(set.getString("CICPOS"), set.getString("CICDIV")));
                                    result.addAll(temp);
                                    OK = true;
                                }
                            } else if (OK) {
                                result.add(new Pair(set.getString("CICPOS"), set.getString("CICDIV")));
                            } else {
                                temp.add(new Pair(set.getString("CICPOS"), set.getString("CICDIV")));
                            }
                        }
                    }

                    if (result.isEmpty()) {
                        result.addAll(temp);
                    }
                    break label164;
                } catch (Exception var20) {
                    log.error(var20.getMessage());
                } finally {
                    if (set != null) {
                        try {
                            set.close();
                            stat.clearParameters();
                            stat.close();
                        } catch (SQLException var19) {
                            log.error(var19.getMessage());
                        }
                    }

                }

                return null;
            }

            if (result.size() == 0) {
                result = null;
            }

            if (result != null) {
                this.getListVOSTROFromBICAndCurrencyResults.put(BIC + " || " + currency, result);
            }

            return result;
        }
    }

    public String getNOSTROFromBICAndCurrency(String BIC, String currency) {
        if (this.getNOSTROFromBICAndCurrencyResults.containsKey("BIC || currency")) {
            return (String)this.getNOSTROFromBICAndCurrencyResults.get("BIC || currency");
        } else {
            BIC = BIC.replaceAll("XXX", "");
            if (BIC.equals("BGALPTPL")) {
                return this.managerParameters.getTargetAccount();
            } else if (BIC.equals("STEP1") && currency.equals("EUR")) {
                return "548914";
            } else if (BIC.equals("TARGET") && currency.equals("EUR")) {
                return this.managerParameters.getTargetAccount();
            } else {
                ResultSet set = null;
                PreparedStatement stat = null;

                String var10;
                try {
                    Pair par = this.getBankAndCountryFromSWIFT(BIC, false);
                    if (par == null) {
                        return null;
                    }

                    String ban = par.getA();
                    String pai = par.getB();
                    if (ban.equals("SCBL") && pai.equals("US")) {
                        ban = "AEIB";
                    }

                    stat = this.conBANKA.prepareStatement(this.context.getNOSTRO());
                    stat.clearParameters();
                    stat.setString(1, "2010" + ban + pai + currency + "%");
                    set = stat.executeQuery();
                    if (set.next() && set.getString("NVCCB").length() > 3) {
                        String result = set.getString("NVCCB");
                        this.getNOSTROFromBICAndCurrencyResults.put(BIC + " || " + currency, result);
                        var10 = result;
                        return var10;
                    }

                    return this.managerParameters.getSemResultados();
                } catch (Exception var20) {
                    log.error(var20.getMessage());
                    var10 = this.managerParameters.getErro();
                } finally {
                    if (set != null) {
                        try {
                            set.close();
                            stat.clearParameters();
                            stat.close();
                        } catch (SQLException var19) {
                            log.error(var19.getMessage());
                        }
                    }

                }

                return var10;
            }
        }
    }

    public LinkedList<Pair> getAllNOSTROFromBICAndCurrency(String BIC, String currency) {
        if (this.getAllNOSTROFromBICAndCurrencyResults.containsKey("BIC || currency")) {
            return (LinkedList)this.getAllNOSTROFromBICAndCurrencyResults.get("BIC || currency");
        } else {
            LinkedList<Pair> temp = new LinkedList();
            LinkedList<Pair> result = new LinkedList();
            if (BIC.equals("BGALPTPL")) {
                result.add(new Pair(this.managerParameters.getTargetAccount(), "EUR"));
                return result;
            } else if (BIC.equals("STEP1")) {
                result.add(new Pair("548914", "EUR"));
                return result;
            } else if (BIC.equals("TARGET")) {
                result.add(new Pair(this.managerParameters.getTargetAccount(), "EUR"));
                return result;
            } else if (BIC.length() < 8) {
                return null;
            } else {
                BIC = BIC.substring(0, 8);
                ResultSet set = null;
                PreparedStatement stat = null;

                LinkedList var12;
                try {
                    Pair par = this.getBankAndCountryFromSWIFT(BIC, false);
                    if (par == null) {
                        return null;
                    }

                    String ban = par.getA();
                    String pai = par.getB();
                    if (ban.equals("SCBL") && pai.equals("US")) {
                        ban = "AEIB";
                    }

                    stat = this.conBANKA.prepareStatement(this.context.getAllNOSTRO());
                    stat.clearParameters();
                    stat.setString(1, "2010" + ban + pai + "%");
                    set = stat.executeQuery();
                    boolean ok = false;

                    while(set.next()) {
                        if (set.getString("NVCCB").length() > 3) {
                            if (!set.getString("nvtccb").substring(10, 13).equals(currency) && !ok) {
                                temp.add(new Pair(set.getString("NVCCB"), set.getString("nvtccb").substring(10, 13)));
                            } else {
                                result.add(new Pair(set.getString("NVCCB"), set.getString("nvtccb").substring(10, 13)));
                                if (!ok) {
                                    result.addAll(temp);
                                    ok = true;
                                }
                            }
                        }
                    }

                    if (result.size() == 0) {
                        result.addAll(temp);
                    }

                    if (result != null) {
                        this.getAllNOSTROFromBICAndCurrencyResults.put(BIC + " || " + currency, result);
                    }

                    var12 = result;
                } catch (Exception var21) {
                    var21.printStackTrace();
                    return null;
                } finally {
                    if (set != null) {
                        try {
                            set.close();
                            stat.clearParameters();
                            stat.close();
                        } catch (SQLException var20) {
                            log.error(var20.getMessage());
                        }
                    }

                }

                return var12;
            }
        }
    }

    public LinkedList<Pair> getALLNOSTROSFromCurrency(String currency) {
        LinkedList<Pair> temp = new LinkedList();
        LinkedList<Pair> result = new LinkedList();
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getAllNOSTRO());
            stat.clearParameters();
            stat.setString(1, "2010%" + currency + "%");
            set = stat.executeQuery();
            boolean ok = false;

            while(set.next()) {
                if (set.getString("NVCCB").trim().length() > 3) {
                    if (!set.getString("nvtccb").substring(10, 13).equals(currency) && !ok) {
                        temp.add(new Pair(set.getString("NVCCB"), set.getString("nvtccb").substring(10, 13)));
                    } else {
                        result.add(new Pair(set.getString("NVCCB"), set.getString("nvtccb").substring(10, 13)));
                        if (!ok) {
                            result.addAll(temp);
                            ok = true;
                        }
                    }
                }
            }

            if (result.size() == 0) {
                result.addAll(temp);
            }

            LinkedList var8 = result;
            return var8;
        } catch (Exception var16) {
            var16.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return null;
    }

    public String getBICfromNOSTRO(String nostro) {
        ResultSet set = null;
        PreparedStatement stat = null;
        if (nostro.equals("548914")) {
            return "2010EBASBEEUR";
        } else {
            try {
                stat = this.conBANKA.prepareStatement(this.context.getBICfromNOSTRO());
                stat.clearParameters();
                stat.setString(1, nostro);
                set = stat.executeQuery();
                if (set.next()) {
                    String var6 = set.getString("NVTCCB");
                    return var6;
                }

                return null;
            } catch (Exception var15) {
                var15.printStackTrace();
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var14) {
                        log.error(var14.getMessage());
                    }
                }

            }

            return null;
        }
    }

    public boolean getLocks(String account) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getLocks());
            stat.clearParameters();
            stat.setString(1, account);
            set = stat.executeQuery();
            if (set.next()) {
                return true;
            }

            return false;
        } catch (Exception var14) {
            var14.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var13) {
                    log.error(var13.getMessage());
                }
            }

        }

        return false;
    }

    public boolean getTaxExemption(String account) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getTaxExemption());
            stat.clearParameters();
            stat.setString(1, account);
            set = stat.executeQuery();
            if (set.next()) {
                return true;
            }
        } catch (Exception var14) {
            var14.printStackTrace();
            return false;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var13) {
                    log.error(var13.getMessage());
                }
            }

        }

        return false;
    }

    public String getCountryFromClient(String client) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getClientInfo());
            stat.clearParameters();
            stat.setString(1, client);
            set = stat.executeQuery();
            if (!set.next()) {
                return null;
            }

            String var6 = set.getString("CLPAIS");
            return var6;
        } catch (Exception var15) {
            var15.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return null;
    }

    public ClientTotalInfo getClientTotalInfo(String client) {
        ClientTotalInfo ct = new ClientTotalInfo();
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getClientTotalInfo());
            stat.clearParameters();
            stat.setString(1, client);
            set = stat.executeQuery();
            if (set.next()) {
                ct.setNumero(set.getString("CLNCLI"));
                ct.setNome(set.getString("TTNOME"));
                ct.setMorada1(set.getString("MOMORD"));
                ct.setMorada2(set.getString("MOMOR1"));
                ct.setLocalidade(set.getString("LOCALIDADE"));
            }

            return ct;
        } catch (Exception var14) {
            var14.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var13) {
                    log.error(var13.getMessage());
                }
            }

        }

        return null;
    }

    public ClientAddresses getClientAddress(String client) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getClientAddresses());
            stat.setString(1, client);
            set = stat.executeQuery();
            ClientAddresses cm = new ClientAddresses();

            while(set.next()) {
                cm.setNome(set.getString("NOME"));
                cm.setMorada(set.getString("MORADA"));
            }

            ClientAddresses var6 = cm;
            return var6;
        } catch (Exception var14) {
            var14.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var13) {
                    log.error(var13.getMessage());
                }
            }

        }

        return null;
    }

    public ClientAccount getClientFromInput(String input) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            ClientAccount cc;
            ClientAccount var6;
            if (input.startsWith("PT50")) {
                if (input.substring(4).trim().isEmpty()) {
                    return null;
                }

                stat = this.conBANKA.prepareStatement(this.context.getClientFromNIB());
                stat.setString(1, input.substring(4));
                set = stat.executeQuery();
                cc = new ClientAccount();
                if (set.next()) {
                    cc.setClientNumber(set.getString("NUMERO"));
                    cc.setClientName(set.getString("NOME"));
                    cc.setAccountNumber(set.getString("CONTA"));
                    cc.setCurrency(set.getString("MOEDA"));
                    cc.setAccountIBAN(input);
                    cc.setBalance(set.getString("SALDO"));
                    var6 = cc;
                    return var6;
                }

                return null;
            }

            try {
                stat = this.conBANKA.prepareStatement(this.context.getClientFromAccount());
                stat.setString(1, input);
                set = stat.executeQuery();
                cc = new ClientAccount();
                if (!set.next()) {
                    return null;
                }

                cc.setClientNumber(set.getString("NUMERO"));
                cc.setClientName(set.getString("NOME"));
                cc.setCurrency(set.getString("MOEDA"));
                cc.setAccountNumber(input);
                cc.setAccountIBAN("PT50" + set.getString("NIB"));
                cc.setBalance(set.getString("SALDO"));
                var6 = cc;
                return var6;
            } catch (Exception var20) {
                return null;
            }
        } catch (Exception var21) {
            var21.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var19) {
                    log.error(var19.getMessage());
                }
            }

        }

        return null;
    }

    public String getNameFromCCB(String account, String currency) {
        if (this.getNameFromCCBResults.containsKey(account)) {
            return (String)this.getNameFromCCBResults.get(account);
        } else {
            ResultSet set = null;
            PreparedStatement stat = null;
            if (account.equals("548914")) {
                return "STEP1";
            } else {
                try {
                    String result = this.getBICfromNOSTRO(account);
                    if (result != null) {
                        String bic = this.getSWIFTFromBankAndCountryAndCurrency(result.substring(4, 8), result.substring(8, 10), result.substring(10, 13));
                        if (bic != null) {
                            this.getNameFromCCBResults.put(account, bic);
                        }

                        String var8 = bic;
                        return var8;
                    }

                    return "CCB";
                } catch (Exception var17) {
                    var17.printStackTrace();
                } finally {
                    if (set != null) {
                        try {
                            ((ResultSet)set).close();
                            ((PreparedStatement)stat).clearParameters();
                            ((PreparedStatement)stat).close();
                        } catch (SQLException var16) {
                            log.error(var16.getMessage());
                        }
                    }

                }

                return null;
            }
        }
    }

    public String getFixing(String from, String to, String date) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getFixing());
            stat.setString(1, from);
            stat.setString(2, to);
            stat.setString(3, from);
            stat.setString(4, to);
            stat.setString(5, date);
            set = stat.executeQuery();
            if (set.next()) {
                String var8 = set.getString(1);
                return var8;
            }
        } catch (Exception var17) {
            var17.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var16) {
                    log.error(var16.getMessage());
                }
            }

        }

        return null;
    }

    public CurrencyCorrespondents getCurrencyCorrespondents(String correspondent, String curr) {
        ResultSet set = null;
        PreparedStatement stat = null;
        LinkedList result = new LinkedList();

        try {
            stat = this.conBANKA.prepareStatement(this.context.getPreferedCorrespondent());
            stat.setString(1, curr);
            set = stat.executeQuery();
            String banco = "";
            String pais = "";

            while(set.next()) {
                if (set.getString(1).length() <= 8) {
                    banco = set.getString(1).trim();
                    pais = set.getString(2).trim();
                }
            }

            if (curr.trim().equals("EUR")) {
                banco = "KRED";
                pais = "BE";
            }

            if (curr.trim().equals("GBP")) {
                banco = "KRED";
                pais = "BE";
            }

            set.close();
            stat.clearParameters();
            stat.close();
            stat = this.conBANKA.prepareStatement(this.context.getCorrespondentByCurrency());
            stat.setString(1, curr);
            stat.setString(2, curr);
            set = stat.executeQuery();
            LinkedList<String> tmp = new LinkedList();
            HashSet<String> already = new HashSet();
            boolean ok = false;
            String swift = "";
            if (correspondent != null && !correspondent.isEmpty()) {
                result.add(correspondent);
            }

            while(true) {
                do {
                    do {
                        if (!set.next()) {
                            if (!tmp.isEmpty()) {
                                result.addAll(tmp);
                            }

                            return new CurrencyCorrespondents(result);
                        }

                        swift = set.getString("DOSWIF").trim();
                        if (swift.length() > 8) {
                            swift = swift.substring(0, 8);
                        }
                    } while(already.contains(swift));
                } while(swift.equals("BAIPPTPL"));

                if (ok || set.getString("NOCBAN").trim().equals(banco) && set.getString("NOSPAI").trim().equals(pais)) {
                    if (!ok) {
                        result.add(swift);
                        result.addAll(tmp);
                        tmp = new LinkedList();
                        ok = true;
                    } else {
                        result.add(swift);
                    }
                } else {
                    tmp.add(swift);
                }

                already.add(swift);
            }
        } catch (Exception var20) {
            var20.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var19) {
                    log.error(var19.getMessage());
                }
            }

        }

        return null;
    }

    public CurrencyCorrespondents getCurrencyCorrespondents(String curr, boolean eba) {
        ResultSet set = null;
        PreparedStatement stat = null;
        LinkedList result = new LinkedList();

        try {
            stat = this.conBANKA.prepareStatement(this.context.getPreferedCorrespondent());
            stat.setString(1, curr);
            set = stat.executeQuery();
            String banco = "";
            String pais = "";

            while(set.next()) {
                if (set.getString(1).length() <= 8) {
                    banco = set.getString(1).trim();
                    pais = set.getString(2).trim();
                }
            }

            if (curr.trim().equals("EUR")) {
                banco = "KRED";
                pais = "BE";
            }

            if (curr.trim().equals("GBP")) {
                banco = "KRED";
                pais = "BE";
            }

            set.close();
            stat.clearParameters();
            stat.close();
            stat = this.conBANKA.prepareStatement(this.context.getCorrespondentByCurrency());
            stat.setString(1, curr);
            stat.setString(2, curr);
            set = stat.executeQuery();
            LinkedList<String> tmp = new LinkedList();
            HashSet<String> already = new HashSet();
            boolean ok = false;
            String swift = "";

            while(true) {
                do {
                    do {
                        if (!set.next()) {
                            if (!tmp.isEmpty() && result.isEmpty()) {
                                result = tmp;
                            }

                            return new CurrencyCorrespondents(result);
                        }

                        swift = set.getString("DOSWIF").trim();
                        if (swift.length() > 8) {
                            swift = swift.substring(0, 8);
                        }
                    } while(already.contains(swift));
                } while(swift.equals("BAIPPTPL"));

                if (ok || set.getString("NOCBAN").trim().equals(banco) && set.getString("NOSPAI").trim().equals(pais)) {
                    if (!ok) {
                        result.add(swift);
                        result.addAll(tmp);
                        ok = true;
                    } else {
                        result.add(swift);
                    }
                } else {
                    tmp.add(swift);
                }

                already.add(swift);
            }
        } catch (Exception var20) {
            var20.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var19) {
                    log.error(var19.getMessage());
                }
            }

        }

        return null;
    }

    public String getCurrencyCode(String currency) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getCurrencyCode());
            stat.setString(1, currency);
            set = stat.executeQuery();
            if (set.next()) {
                String var6 = set.getString(1);
                return var6;
            }
        } catch (Exception var15) {
            var15.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return null;
    }

    public Pair getBankAndCountryFromSWIFT(String BIC, boolean offline) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            Pair var7;
            if (BIC.equals("XXXXXXXX")) {
                var7 = new Pair("XXXX", "XX");
                return var7;
            }

            stat = this.conBANKA.prepareStatement(this.context.getCaractegorizationSWIFTOffline());
            stat.setString(1, BIC + "%");
            set = stat.executeQuery();
            if (!set.next()) {
                return null;
            }

            var7 = new Pair(set.getString("NOCBAN"), set.getString("NOSPAI"));
            return var7;
        } catch (Exception var17) {
            var17.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var16) {
                    log.error(var16.getMessage());
                }
            }

        }

        return null;
    }

    public String getSWIFTFromBankAndCountryAndCurrency(String bank, String country, String currency) {
        ResultSet set = null;
        PreparedStatement stat = null;
        if (bank.equals("EBAS") && country.equals("BE") && currency.equals("EUR")) {
            return "EBASBEBB";
        } else {
            try {
                stat = this.conBANKA.prepareStatement(this.context.getCaracterizationBankCountry());
                stat.setString(1, bank);
                stat.setString(2, country);
                stat.setString(3, currency);
                set = stat.executeQuery();
                if (set.next()) {
                    String var8 = set.getString("DOSWIF");
                    return var8;
                }
            } catch (Exception var17) {
                var17.printStackTrace();
                return null;
            } finally {
                if (set != null) {
                    try {
                        set.close();
                        stat.clearParameters();
                        stat.close();
                    } catch (SQLException var16) {
                        log.error(var16.getMessage());
                    }
                }

            }

            return null;
        }
    }

    public LinkedList<SWIFTMsg> getSWIFTFields(String tipo, boolean header) {
        LinkedList<SWIFTMsg> result = new LinkedList();
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getSWIFTFields());
            stat.setString(1, tipo);
            set = stat.executeQuery();
            if (header) {
                result.add(new SWIFTMsg("HD1 ", "Message Type                  ", "REGRA", "A", 0, tipo + StringUtils.repeat(" ", 65 - tipo.length()), "N"));
                result.add(new SWIFTMsg("HD2 ", "Sender                        ", "REGRA", "A", 0, this.managerParameters.getBICBAI() + StringUtils.repeat(" ", 65 - this.managerParameters.getBICBAI().length()), "N"));
                result.add(new SWIFTMsg("HD3 ", "Receiver                      ", "REGRA", "A", 0, this.managerParameters.getBICBAI() + StringUtils.repeat(" ", 65 - this.managerParameters.getBICBAI().length()), "S"));
            }

            while(set.next()) {
                for(int i = 0; i < (set.getInt("MSNOCR") == 0 ? 1 : set.getInt("MSNOCR")); ++i) {
                    result.add(new SWIFTMsg(set.getString("MSETIQ"), set.getString("MSDETQ"), set.getString("MSVETQ"), set.getString("MSTIPE"), set.getInt("MSCMPE"), "", set.getString("MSMAND")));
                }
            }

            return result;
        } catch (Exception var15) {
            var15.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    set = null;
                    stat.clearParameters();
                    stat.close();
                    stat = null;
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return null;
    }

    public Pair getSWIFTMsg(String type, String operation) {
        ResultSet set = null;
        PreparedStatement stat = null;

        Pair var7;
        try {
            stat = this.conBANKA.prepareStatement(this.context.getSwiftMsg());
            stat.setString(1, type);
            stat.setString(2, operation);
            set = stat.executeQuery();
            if (!set.next()) {
                return null;
            }

            var7 = new Pair(set.getString(1), set.getString(2));
        } catch (Exception var16) {
            var16.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return var7;
    }

    public Pair getBranchAndAccountFromOperation(String operation) {
        ResultSet set = null;
        PreparedStatement stat = null;

        Pair var6;
        try {
            stat = this.conBANKA.prepareStatement(this.context.getAccountFromOperationNumber());
            stat.setString(1, operation);
            set = stat.executeQuery();
            if (!set.next()) {
                return null;
            }

            var6 = new Pair(set.getString("mvbal"), set.getString("GBMVCONTA"));
        } catch (Exception var15) {
            var15.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return var6;
    }

    public Quad getAccountFromOperationNumberComissions(String operation, boolean comission) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getAccountFromOperationNumberComissions());
            stat.setString(1, operation);
            set = stat.executeQuery();

            while(set.next()) {
                Quad var7;
                if (comission) {
                    if (set.getInt("mvcope") >= 250) {
                        var7 = new Quad(set.getString("GBMVCONTA"), set.getString("mvmoed"), set.getString("mvdmov"), set.getString("mvbal"));
                        return var7;
                    }
                } else if (set.getInt("mvcope") < 250) {
                    var7 = new Quad(set.getString("GBMVCONTA"), set.getString("mvmoed"), set.getString("mvdmov"), set.getString("mvbal"));
                    return var7;
                }
            }
        } catch (Exception var17) {
            var17.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var16) {
                    log.error(var16.getMessage());
                }
            }

        }

        return null;
    }

    public String getDataBanka() {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getDataBanka());
            set = stat.executeQuery();
            if (set.next()) {
                String var5 = set.getString(1);
                return var5;
            }

            return null;
        } catch (Exception var14) {
            var14.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var13) {
                    log.error(var13.getMessage());
                }
            }

        }

        return null;
    }

    public String getAuthorizationCode(String user) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getAuthorizationCode());
            stat.setString(1, user);
            set = stat.executeQuery();
            if (set.next()) {
                String var6 = set.getString(1);
                return var6;
            }
        } catch (Exception var15) {
            var15.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return null;
    }

    public List<ApiFieldBO> getApiExtraFields(String tipoEnt, String viewEntP) throws Exception {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getApiExtraFields());
            stat.setString(1, tipoEnt);
            stat.setString(2, viewEntP);
            set = stat.executeQuery();
            ArrayList apiFields = new ArrayList();

            while(set.next()) {
                apiFields.add(new ApiFieldBO(set.getString(1), set.getString(2), set.getString(3)));
            }

            ArrayList var7 = apiFields;
            return var7;
        } catch (Exception var14) {
            throw var14;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    if (!stat.isClosed()) {
                        stat.clearParameters();
                        stat.close();
                    }
                } catch (SQLException var13) {
                    throw var13;
                }
            }

        }
    }

    public boolean getValidDate(String currency, String date) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            if (currency.equals("EUR")) {
                stat = this.conBANKA.prepareStatement(this.context.getValidDayEUR());
                stat.setString(1, date);
                stat.setString(2, date);
            } else {
                stat = this.conBANKA.prepareStatement(this.context.getValidDayNotEUR());
                stat.setString(1, currency);
                stat.setString(2, date);
                stat.setString(3, date);
            }

            set = stat.executeQuery();
            if (!set.next()) {
                return false;
            }

            boolean var7 = set.getInt(1) == 0;
            return var7;
        } catch (Exception var16) {
            var16.printStackTrace();
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return false;
    }

    public boolean isClientBank(String client) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.isClientBank());
            stat.setString(1, client);
            set = stat.executeQuery();

            do {
                if (!set.next()) {
                    return false;
                }
            } while(!set.getString(1).trim().equals("S") && !set.getString(1).trim().equals("B"));

            return true;
        } catch (Exception var14) {
            log.error(var14.getMessage());
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var13) {
                    log.error(var13.getMessage());
                }
            }

        }

        return false;
    }

    public String getOperationDone(String input, String input2) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getOperationDone());
            stat.setString(1, input);
            set = stat.executeQuery();
            if (set.next()) {
                String var7 = set.getString(1);
                return var7;
            }
        } catch (Exception var16) {
            var16.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return null;
    }

    public boolean getOperationDoneToday(String input, String input2, String date) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getOperationDoneToday());
            stat.setString(1, input);
            stat.setString(2, input2);
            stat.setString(3, date);
            set = stat.executeQuery();
            if (!set.next()) {
                return false;
            }

            return true;
        } catch (Exception var16) {
            log.error(var16.getMessage());
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return false;
    }

    public HashSet<String> getOperationsCorrespondent(String input) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getOperationsCorrespondent());
            stat.setString(1, input);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
            Calendar cal = Calendar.getInstance();
            cal.add(2, -1);
            stat.setString(2, sdf.format(cal.getTime()) + "01");
            set = stat.executeQuery();
            HashSet operationNumbers = new HashSet();

            while(set.next()) {
                operationNumbers.add(set.getString(1));
            }

            HashSet var8 = operationNumbers;
            return var8;
        } catch (Exception var16) {
            log.error(var16.getMessage());
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return null;
    }

    public LinkedList<OperationWrapper> getOperationsCO(String date, String type) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getOperationsCO());
            stat.setInt(1, Integer.parseInt(date));
            stat.setString(2, type + "%");
            set = stat.executeQuery();
            LinkedList operations = new LinkedList();

            while(set.next()) {
                operations.add(new OperationWrapper(set.getString(1), set.getString(2), set.getString(3), set.getString(4), set.getString(5), set.getString(6), set.getString(7)));
            }

            LinkedList var7 = operations;
            return var7;
        } catch (Exception var15) {
            log.error(var15.getMessage());
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return null;
    }

    public LinkedList<OperationWrapper> getOperationsCOOthers(String date, String user, String type) {
        ResultSet set = null;
        PreparedStatement stat = null;

        try {
            stat = this.conBANKA.prepareStatement(this.context.getOperationsCOOthers());
            stat.setInt(1, Integer.parseInt(date));
            stat.setString(2, user);
            stat.setString(3, type);
            set = stat.executeQuery();
            LinkedList operations = new LinkedList();

            while(set.next()) {
                operations.add(new OperationWrapper(set.getString(1), set.getString(2), set.getString(3), set.getString(4), set.getString(5), set.getString(6)));
            }

            LinkedList var8 = operations;
            return var8;
        } catch (Exception var16) {
            log.error(var16.getMessage());
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return null;
    }

    public String[] getOperationFGEXTMM(String operation_nr) {
        ResultSet set = null;
        PreparedStatement stat = null;

        String[] var6;
        try {
            String[] result = null;
            stat = this.conBANKA.prepareStatement(this.context.getOperationFGEXTMM());
            stat.setString(1, operation_nr);
            set = stat.executeQuery();
            if (set.next()) {
                result = new String[]{set.getString(1), set.getString(2), set.getString(3), set.getString(4), set.getString(5), set.getString(6), set.getString(7)};
                var6 = result;
                return var6;
            }

            var6 = result;
        } catch (Exception var15) {
            var15.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return var6;
    }

    public String getClientByName(String nome) {
        ResultSet set = null;
        PreparedStatement stat = null;

        String var6;
        try {
            stat = this.conBANKA.prepareStatement(this.context.getClientByName());
            stat.setString(1, nome);
            set = stat.executeQuery();
            if (!set.next()) {
                return null;
            }

            var6 = set.getString(1);
        } catch (Exception var15) {
            var15.printStackTrace();
            return null;
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return var6;
    }

    public HashSet<String> getTableResults(String table) {
        ResultSet set = null;
        PreparedStatement stat = null;
        HashSet result = new HashSet();

        try {
            stat = this.conBANKA.prepareStatement(this.context.getTableResults(table));
            set = stat.executeQuery();

            while(set.next()) {
                result.add(set.getString(1));
            }

            HashSet var7 = result;
            return var7;
        } catch (Exception var15) {
            log.error(var15.getMessage());
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var14) {
                    log.error(var14.getMessage());
                }
            }

        }

        return null;
    }

    public String[] getBICRules(String bic, String currency) {
        ResultSet set = null;
        PreparedStatement stat = null;
        String[] result = new String[2];

        try {
            stat = this.conBANKA.prepareStatement(this.context.getBicResults(bic, currency));
            stat.setString(1, bic);
            stat.setString(2, currency);

            for(set = stat.executeQuery(); set.next(); result[1] = set.getString(3)) {
                result[0] = set.getString(2);
            }

            String[] var8 = result;
            return var8;
        } catch (Exception var16) {
            log.error(var16.getMessage());
        } finally {
            if (set != null) {
                try {
                    set.close();
                    stat.clearParameters();
                    stat.close();
                } catch (SQLException var15) {
                    log.error(var15.getMessage());
                }
            }

        }

        return null;
    }

    public String getCorrespondente(String moeda) {
        try {
            ResultSet set = null;
            PreparedStatement stat = null;
            stat = this.conBANKA.prepareStatement(this.context.getCorrespondente());
            stat.clearParameters();
            stat.setString(1, moeda);
            set = stat.executeQuery();
            return set.next() ? set.getString(1) : null;
        } catch (SQLException var4) {
            log.error(var4.getMessage());
            return null;
        }
    }

    public boolean isVostro(String conta, String moeda) {
        try {
            ResultSet set = null;
            PreparedStatement stat = null;
            stat = this.conBANKA.prepareStatement(this.context.getIsVostro());
            stat.clearParameters();
            stat.setString(1, conta);
            stat.setString(2, moeda);
            set = stat.executeQuery();
            return set.next();
        } catch (SQLException var5) {
            log.error(var5.getMessage());
            return false;
        }
    }

    public ArrayList<String> getCambiosDivisas(String CBDATC, String CBMOED, String CBMOEB2, String CBTCOT) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getCambiosDivisas());
            query.setString(1, CBDATC);
            query.setString(2, CBMOED);
            query.setString(3, CBMOEB2);
            query.setString(4, CBTCOT);
            resultSet = query.executeQuery();
            resultSet.next();
            ArrayList<String> resultList = new ArrayList();
            resultList.add(resultSet.getString("CBVEND"));
            resultList.add(resultSet.getString("CBCOMP"));
            return resultList;
        } catch (SQLException var8) {
            log.error(var8.getMessage());
            return null;
        }
    }

    public String getTaxas(String tipo, String data) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getTaxas());
            query.setString(1, tipo);
            query.setString(2, data);
            resultSet = query.executeQuery();
            resultSet.next();
            String taxa = resultSet.getString("TVTAXA");
            return taxa;
        } catch (SQLException var6) {
            log.error(var6.getMessage());
            return null;
        }
    }

    public List<String> getEntityByName(String name) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getEntityByName());
            query.setString(1, name);
            resultSet = query.executeQuery();
            resultSet.next();
            List<String> entity = new ArrayList();

            for(int i = 1; i <= resultSet.getMetaData().getColumnCount(); ++i) {
                entity.add(resultSet.getString(resultSet.getMetaData().getColumnName(i)));
            }

            return entity;
        } catch (SQLException var6) {
            log.error(var6.getMessage());
            return null;
        }
    }

    public boolean getEntityByDocTypeAndDocNumber(int docType, String docNumber) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getEntityByDocTypeAndNumber());
            query.setInt(1, docType);
            query.setString(2, docNumber);
            resultSet = query.executeQuery();
            return resultSet.next();
        } catch (SQLException var5) {
            log.error(var5.getMessage());
            return false;
        }
    }

    public boolean getEntBloqAML(int numEnt) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getEntBloqAML());
            query.setInt(1, numEnt);
            resultSet = query.executeQuery();
            return resultSet.next();
        } catch (SQLException var4) {
            log.error(var4.getMessage());
            return false;
        }
    }

    public String getDtEndDp(long dpAccountNum) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getDpDetails());
            query.setLong(1, dpAccountNum);
            resultSet = query.executeQuery();
            resultSet.next();
            return resultSet.getString("DPDVNC");
        } catch (SQLException var5) {
            log.error(var5.getMessage());
            return null;
        }
    }

    public BigDecimal getDpBalance(long dpAccountNum) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getDpDetails());
            query.setLong(1, dpAccountNum);
            resultSet = query.executeQuery();
            resultSet.next();
            return resultSet.getBigDecimal("DPSLD");
        } catch (SQLException var5) {
            log.error(var5.getMessage());
            return null;
        }
    }

    public List<Long> getDpMovementList(long accountNr, int copeNr) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getDpMovement());
            query.setLong(1, accountNr);
            query.setInt(2, copeNr);
            resultSet = query.executeQuery();
            ArrayList entity = new ArrayList();

            while(resultSet.next()) {
                entity.add(resultSet.getLong("mpdatl"));
            }

            return entity;
        } catch (SQLException var7) {
            log.error(var7.getMessage());
            return null;
        }
    }

    public List<BigDecimal> getEndDpMovement(long accountNr, int copeNr) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getDoEndDPMovement());
            query.setLong(1, accountNr);
            query.setInt(2, copeNr);
            resultSet = query.executeQuery();
            ArrayList entity = new ArrayList();

            while(resultSet.next()) {
                entity.add(resultSet.getBigDecimal(1));
            }

            return entity;
        } catch (SQLException var7) {
            log.error(var7.getMessage());
            return null;
        }
    }

    public List<BigDecimal> getDpEndDPMovement(long accountNr) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getDpEndDPMovement());
            query.setLong(1, accountNr);
            resultSet = query.executeQuery();
            ArrayList entity = new ArrayList();

            while(resultSet.next()) {
                entity.add(resultSet.getBigDecimal(1));
            }

            return entity;
        } catch (SQLException var6) {
            log.error(var6.getMessage());
            return null;
        }
    }

    public Integer getNrClienteByEntityNr(int entityNr) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getNrClienteByEntityNr());
            query.setInt(1, entityNr);
            resultSet = query.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException var4) {
            log.error(var4.getMessage());
            return null;
        }
    }

    public Long getDoByClientNr(int clientNr) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getDoByClientNr());
            query.setInt(1, clientNr);
            resultSet = query.executeQuery();
            resultSet.next();
            return resultSet.getLong(1);
        } catch (SQLException var4) {
            log.error(var4.getMessage());
            return null;
        }
    }

    public ArrayList<String> getCambiosDivisasLatest(String CBMOED, String CBMOEB2, String CBTCOT) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getCambiosDivisasLatest());
            query.setString(1, CBMOED);
            query.setString(2, CBMOEB2);
            query.setString(3, CBTCOT);
            resultSet = query.executeQuery();
            resultSet.next();
            ArrayList<String> resultList = new ArrayList();
            resultList.add(resultSet.getString("CBVEND"));
            resultList.add(resultSet.getString("CBCOMP"));
            return resultList;
        } catch (SQLException var7) {
            log.error(var7.getMessage());
            return null;
        }
    }

    public String getTaxasLatest(String tipo) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getTaxasLatest());
            query.setString(1, tipo);
            resultSet = query.executeQuery();
            resultSet.next();
            String taxa = resultSet.getString("TVTAXA");
            return taxa;
        } catch (SQLException var5) {
            log.error(var5.getMessage());
            return null;
        }
    }

    public Long getDpAccountNumber(int clientNr, long accountDo, String description) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getDpAccountNumber());
            query.setInt(1, clientNr);
            query.setLong(2, accountDo);
            query.setString(3, description);
            resultSet = query.executeQuery();
            resultSet.next();
            return resultSet.getLong(1);
        } catch (SQLException var7) {
            log.error(var7.getMessage());
            return null;
        }
    }

    public PairWithGenerics<Long, BigDecimal> getSepaNumber(String tipoTrans, long accountDo, String ibanDest, String observacoes) {
        try {
            ResultSet resultSet = null;
            PreparedStatement query = null;
            query = this.conBANKA.prepareStatement(this.context.getSepaNumber());
            query.setString(1, tipoTrans);
            query.setLong(2, accountDo);
            query.setString(3, ibanDest);
            query.setString(4, observacoes);
            resultSet = query.executeQuery();
            resultSet.next();
            return new PairWithGenerics(resultSet.getLong(1), resultSet.getBigDecimal(2));
        } catch (SQLException var8) {
            log.error(var8.getMessage());
            return null;
        }
    }
}
