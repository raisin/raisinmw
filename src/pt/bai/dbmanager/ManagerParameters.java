/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.bai.dbmanager;

/**
 *
 * @author loliveira
 */
public class ManagerParameters {

    private String BICBAI;

    private String erro = "ERRO";

    private String semResultados = "Sem resultados";

    private String targetAccount = "10105";

    private String STEP1tag = "STEP1";

    private String TARGETtag = "TARGET";

    public String getBICBAI() {
        if (BICBAI == null) {
            return "BAIPPTPLAXXX";
        }
        return BICBAI;
    }

    public void setBICBAI(String BICBAI) {
        this.BICBAI = BICBAI;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getSemResultados() {
        return semResultados;
    }

    public void setSemResultados(String semResultados) {
        this.semResultados = semResultados;
    }

    public String getTargetAccount() {
        return targetAccount;
    }

    public void setTargetAccount(String targetAccount) {
        this.targetAccount = targetAccount;
    }

    public String getSTEP1tag() {
        return STEP1tag;
    }

    public void setSTEP1tag(String STEP1tag) {
        this.STEP1tag = STEP1tag;
    }

    public String getTARGETtag() {
        return TARGETtag;
    }

    public void setTARGETtag(String TARGETtag) {
        this.TARGETtag = TARGETtag;
    }

}