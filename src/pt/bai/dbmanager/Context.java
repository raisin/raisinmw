package pt.bai.dbmanager;

public class Context {

    public String LibVersion;

    public String getCaractegorizationSWIFT() {
        return "select "
                + "case when CICDIV is null then NOCDIV else CICDIV end as NOCDIV, "
                + "docban as NOCBAN, "
                + "NOSPAI, "
                + "DOSWIF, "
                + "NOIBAN, "
                + "cicpos as NOCVOS, "
                + "NOCNOS "
                + "from PROFG" + LibVersion + ".FGDOMIC "
                + "left join MMM" + LibVersion + ".MMCNTI on CICBAN = DOCBAN and CISPAI = DOSPAI and CICBAL = DOCBAL and CICDOM = DOCDOM and DOKSWF = 'S' "
                + "left join profg" + LibVersion + ".fgnost on DOCBAN = NOCBAN AND DOSPAI = NOSPAI AND DOCBAL = NOCBAL AND DOCDOM = NOCDOM and nositu = 'V' "
                + "where DOSWIF like ? and dositu = 'V' and NOSPAI is not null with UR";
    }

    public String getCaractegorizationSWIFTOffline() {
        return "select "
                + "case when CICDIV is null then NOCDIV else CICDIV end as NOCDIV, "
                + "docban as NOCBAN, "
                + "NOSPAI, "
                + "DOSWIF, "
                + "NOIBAN, "
                + "cicpos as NOCVOS, "
                + "NOCNOS "
                + "from sispaga.FGDOMIC "
                + "left join sispaga.MMCNTI on CICBAN = DOCBAN and CISPAI = DOSPAI and CICBAL = DOCBAL and CICDOM = DOCDOM and DOKSWF = 'S' "
                + "left join sispaga.fgnost on DOCBAN = NOCBAN AND DOSPAI = NOSPAI AND DOCBAL = NOCBAL AND DOCDOM = NOCDOM and nositu = 'V' "
                + "where DOSWIF like ? and dositu = 'V' and NOSPAI is not null with UR";
    }

    public String getCaracterizationBankCountry() {
        return "select "
                + "case when CICDIV is null then NOCDIV else CICDIV end as NOCDIV, "
                + "docban as NOCBAN, "
                + "NOSPAI, "
                + "DOSWIF, "
                + "NOIBAN, "
                + "cicpos as NOCVOS, "
                + "NOCNOS "
                + "from sispaga.FGDOMIC "
                + "left join sispaga.MMCNTI on CICBAN = DOCBAN and CISPAI = DOSPAI and CICBAL = DOCBAL and CICDOM = DOCDOM and DOKSWF = 'S'  "
                + "left join sispaga.fgnost on DOCBAN = NOCBAN AND DOSPAI = NOSPAI AND DOCBAL = NOCBAL AND DOCDOM = NOCDOM and nositu = 'V' "
                + "where NOCBAN = ? and NOSPAI = ? and NOCDIV = ? and dositu = 'V' with UR";
    }

    public String getOperationDescription() {
        return "select COCOPE,CODESC from gbanca" + LibVersion + ".gbcodop where COSITU = 'V' with UR";
    }

    public String getNOSTRO() {
        return "select * from PROFG" + LibVersion + ".FGCCBDV where fgnvsitu = 'V'  and nvtccb like ? with UR";
    }

    public String getBankCountryFromCCB() {
        return "select * from PROFG" + LibVersion + ".FGCCBDV where fgnvsitu = 'V' and NVCCB like ? and nvtccb like '2010%' with UR";
    }

    public String getAllNOSTRO() {
        return "select * from PROFG" + LibVersion + ".FGCCBDV where fgnvsitu = 'V' and nvtccb like ? with UR";
    }

    public String getBICfromNOSTRO() {
        return "select * from PROFG" + LibVersion + ".FGCCBDV where fgnvsitu = 'V'  and nvtccb like '2010%' and nvccb = ? with UR";
    }

    public String getVOSTRO() {
        return "select CICPOS,CICDIV from sispaga.MMCNTI where CICBAN = ? and CISPAI = ? order by CICDIV with UR";
    }

    public String getVOSTROOffline() {
        return "select CICPOS,CICDIV from sispaga.MMCNTI where CICBAN = ? and CISPAI = ? order by CICDIV with UR";
    }

    public String getVOSTROcurrency() {
        return "select CICPOS,CICDIV from sispaga.MMCNTI where CICBAN = ? and CISPAI = ? and CICDIV = ? order by CICDIV with UR";
    }

    public String getVOSTROcurrencyOffline() {
        return "select CICPOS,CICDIV from sispaga.MMCNTI where CICBAN = ? and CISPAI = ? and CICDIV = ? order by CICDIV with UR";
    }

    //    public String getBICfromVOSTRO() {
//        return "select CICBAN , CISPAI from MMM" + LibVersion + ".MMCNTI where CICPOS like ? with UR";
//    }
    public String getBICfromVOSTRO() {
        return "select  "
                + "DOSWIF "
                + "from sispaga.FGDOMIC, sispaga.MMCNTI "
                + "where  "
                + "dositu = 'V' and "
                + "CICBAN = docban and "
                + "CISPAI = dospai and "
                + "CICBAL = docbal and "
                + "CICDOM = docdom and "
                + "DOKSWF = 'S' and "
                + "cicpos like ? with UR";
    }

    public String getAccountNIB() {
        return "select \n"
                + "	distinct DONCLI AS CLIENTE, \n"
                + "	TTNUMT AS ENTIDADE,\n"
                + "	GBDOCONTA AS CONTA, \n"
                + "	DOMOED AS MOEDA,\n"
                + "	TTNOMT AS NOME,\n"
                + "	DONIB AS NIB,\n"
                + "	DOBAL AS BALCAO,"
                + "     DOSLD + abs(dolimd) - (case when D.cativo is null then 0 else D.cativo end) AS SALDO,\n"
                + "	case when A.MOMORD is null then B.MOMORD when B.MOMORD is null then B.MOMORD else A.MOMORD end as MORADA1,\n"
                + "	case when A.MOMOR1 is null then B.MOMOR1 else A.MOMOR1 end as MORADA2,\n"
                + "	case when CodexA.$XDCOD is null then CodexB.$XDCOD else CodexA.$XDCOD end as CODEX,\n"
                + "	case when contractoib.BRCATIPO is null then 'N' else 'S' end as IB,\n"
                + "     CLPAIS as PAIS\n"
                + "from \n"
                + "	gbanca" + LibVersion + ".gbcdo\n"
                + "	left join gbanca" + LibVersion + ".gbcli3 ON doncli = clncli\n"
                + "	left join gbanca" + LibVersion + ".gbmord A ON A.motmor ='N' and A.moncli = clncli and A.gbmositu = 'V'\n"
                + "	left join gbanca" + LibVersion + ".gbmord B ON B.motmor ='C' and B.moncli = clncli and B.gbmositu = 'V'\n"
                + "	left join gbanca" + LibVersion + ".gbmord C ON C.motmor ='T' and C.montit = TTNUMT and B.gbmositu = 'V'\n"
                + "     left join (select gbcaconta, sum(cavlr) as cativo from gbanca" + LibVersion + ".GBMCTV where casitu = 'N' group by gbcaconta) D ON D.gbcaconta = gbdoconta\n"
                + "	left join promof" + LibVersion + ".$pcodex CodexA on A.mocodx= CodexA.$xcodx	\n"
                + "	left join promof" + LibVersion + ".$pcodex CodexB on B.mocodx= CodexB.$xcodx	\n"
                + "	left join promof" + LibVersion + ".$pcodex CodexC on C.mocodx= CodexC.$xcodx	\n"
                + "	left join bancar" + LibVersion + ".brcades contractoib on ttnumt = brcanumt and brcasitu = 'V'	\n"
                + "WHERE DONIB = ? with UR";
    }

    public String getDOAccountInfo() {
        return "select \n"
                + "	distinct DONCLI AS CLIENTE, \n"
                + "	TTNUMT AS ENTIDADE,\n"
                + "	GBDOCONTA AS CONTA, \n"
                + "	DOMOED AS MOEDA,\n"
                + "	TTNOMT AS NOME,\n"
                + "	DONIB AS NIB,\n"
                + "	DOBAL AS BALCAO,\n"
                + "	DOSLD + abs(dolimd) - (case when D.cativo is null then 0 else D.cativo end) AS SALDO,\n"
                + "	case when A.MOMORD is null then B.MOMORD when B.MOMORD is null then B.MOMORD else A.MOMORD end as MORADA1,\n"
                + "	case when A.MOMOR1 is null then B.MOMOR1 else A.MOMOR1 end as MORADA2,\n"
                + "	case when CodexA.$XDCOD is null then CodexB.$XDCOD else CodexA.$XDCOD end as CODEX,\n"
                + "	case when contractoib.BRCATIPO is null then 'N' else 'S' end as IB,"
                + "     CLPAIS as PAIS\n"
                + "from \n"
                + "	gbanca" + LibVersion + ".gbcdo\n"
                + "	left join gbanca" + LibVersion + ".gbcli3 ON doncli = clncli\n"
                + "	left join gbanca" + LibVersion + ".gbmord A ON A.motmor ='N' and A.moncli = clncli and A.gbmositu = 'V'\n"
                + "	left join gbanca" + LibVersion + ".gbmord B ON B.motmor ='C' and B.moncli = clncli and B.gbmositu = 'V'\n"
                + "	left join gbanca" + LibVersion + ".gbmord C ON C.motmor ='T' and C.montit = TTNUMT and B.gbmositu = 'V'\n"
                + "     left join (select gbcaconta, sum(cavlr) as cativo from gbanca" + LibVersion + ".GBMCTV where casitu = 'N' group by gbcaconta) D ON D.gbcaconta = gbdoconta\n"
                + "	left join promof" + LibVersion + ".$pcodex CodexA on A.mocodx= CodexA.$xcodx	\n"
                + "	left join promof" + LibVersion + ".$pcodex CodexB on B.mocodx= CodexB.$xcodx	\n"
                + "	left join promof" + LibVersion + ".$pcodex CodexC on C.mocodx= CodexC.$xcodx	\n"
                + "	left join bancar" + LibVersion + ".brcades contractoib on ttnumt = brcanumt and brcasitu = 'V'	\n"
                + "WHERE GBDOCONTA = ? with UR";
    }

    public String getLocks() {
        return "select * from GBANCA" + LibVersion + ".GBBLOQ where (GBBQCLCP = 'DO' or GBBQCLCP = '') and BQSITU = 'V' and BQNCLI = ? with UR";
    }

    public String getTaxExemption() {
        return "select * from GBANCA" + LibVersion + ".GBISENC where istisn = 'IS' and issitu = 'V' and (GBISCLCP = '' or GBISCLCP = 'DO') and isncli = ? with UR";
    }

    public String getClientInfo() {
        return "select * from GBANCA" + LibVersion + ".GBCLI where clncli = ? with UR";
    }

    public String getClientTotalInfo() {
        return "select \n"
                + "distinct clncli,\n"
                + "ttnome,\n"
                + "MOMORD,\n"
                + "momor1,\n"
                + "$xdcod as	localidade\n"
                + "from \n"
                + "(SELECT \n"
                + "* \n"
                + "FROM \n"
                + "gbanca" + LibVersion + ".gbcdo left join (SELECT \n"
                + "CLI3.CLNCLI, \n"
                + "CLI3.TTNUMT, \n"
                + "CLI3.TTNOME \n"
                + "FROM\n"
                + "gbanca" + LibVersion + ".gbcli3 CLI3, \n"
                + "gbanca" + LibVersion + ".gbtit TIT\n"
                + "WHERE \n"
                + "CLI3.TTNUMT = TIT.TTNUMT) as clientEnt on	clientEnt.clncli=doncli \n"
                + "where \n"
                + "dositu not in ('E','F') and \n"
                + "dobal!=9003) as clienteAct left join (select \n"
                + "* \n"
                + "from (SELECT \n"
                + "TC.MONCLI, \n"
                + "TC.MOTMOR,\n"
                + "TC.MOMORD,\n"
                + "tc.momor1,\n"
                + "tc.mocodx, \n"
                + "tc.gbmositu \n"
                + "FROM \n"
                + "gbanca" + LibVersion + ".gbmord TC LEFT JOIN gbanca" + LibVersion + ".gbmord TN ON TC.MONCLI = TN.MONCLI AND\n"
                + "TC.MOTMOR = 'C' AND\n"
                + "TN.MOTMOR = 'N' and \n"
                + "tn.gbmositu='V' \n"
                + "WHERE \n"
                + "tc.motmor in('C','N') and\n"
                + "tn.moncli is null and \n"
                + "tc.gbmositu='V') morada left join promof" + LibVersion + ".$pcodex on mocodx= $xcodx) moradaCod on moncli = clncli\n"
                + "WHERE clncli = ?\n with UR";
    }

    public String getClientAddresses() {
        return "SELECT \n"
                + "MONCLI CLIENTE,\n"
                + "TTNOMT NOME,\n"
                + "CONCAT(CONCAT(CONCAT(CONCAT(TRIM(MOMORD),' '),TRIM(MOMOR1)),' '),TRIM($XDCOD)) MORADA\n"
                + "FROM \n"
                + "GBANCA" + LibVersion + ".GBCLI\n"
                + "LEFT JOIN GBANCA" + LibVersion + ".GBLTIT ON CLNCLI = LTNCLI\n"
                + "LEFT JOIN GBANCA" + LibVersion + ".GBTIT ON TTNUMT = LTNTIT \n"
                + "LEFT JOIN GBANCA" + LibVersion + ".GBMORD ON CLNCLI = MONCLI \n"
                + "LEFT JOIN PROMOF" + LibVersion + ".$PCODEX ON $XCODX = MOCODX \n"
                + "WHERE \n"
                + "GBMOSITU = 'V' AND\n"
                + "MOTMOR = 'C' AND\n"
                + "LTRCLI ='01' AND\n"
                + "CLNCLI = ?\n with UR";
    }

    public String getClientFromAccount() {
        return "select doncli as NUMERO, cliente.ttnomt as NOME , conta.DONIB as NIB, conta.domoed MOEDA, "
                + "	DOSLD + abs(dolimd) - (case when D.cativo is null then 0 else D.cativo end) AS SALDO\n"
                + "from \n"
                + "gbanca" + LibVersion + ".gbcdo conta\n"
                + "left join gbanca" + LibVersion + ".gbcli3 cliente on conta.doncli = cliente.clncli\n"
                + "     left join (select gbcaconta, sum(cavlr) as cativo from gbanca" + LibVersion + ".GBMCTV where casitu = 'N' group by gbcaconta) D ON D.gbcaconta = gbdoconta\n"
                + "where conta.gbdoconta = ? with UR";
    }

    public String getClientFromNIB() {
        return "select doncli as NUMERO, cliente.ttnomt as NOME, conta.gbdoconta as CONTA, conta.domoed MOEDA,  "
                + "	DOSLD + abs(dolimd) - (case when D.cativo is null then 0 else D.cativo end) AS SALDO\n"
                + "from \n"
                + "gbanca" + LibVersion + ".gbcdo conta\n"
                + "left join gbanca" + LibVersion + ".gbcli3 cliente on conta.doncli = cliente.clncli\n"
                + "     left join (select gbcaconta, sum(cavlr) as cativo from gbanca" + LibVersion + ".GBMCTV where casitu = 'N' group by gbcaconta) D ON D.gbcaconta = gbdoconta\n"
                + "where DONIB = ? with UR";
    }

    public String getNameFromCCB() {
        return "SELECT \n"
                + "    pndesc \n"
                + "FROM \n"
                + "    SGCONT" + LibVersion + ".CTPLANO\n"
                + "WHERE\n"
                + "   PNPLN = ? and\n"
                + "   pncnta = ? with UR";
    }

    public String getFixing() {
        return "SELECT \n"
                + "A.CBVEND\n"
                + "FROM\n"
                + "PROFG" + LibVersion + ".FGCMB A\n"
                + "WHERE\n"
                + "A.CBMOED = ? AND \n"
                + "A.CBMOEB = ? AND \n"
                + "A.CBTCOT = 'F' AND \n"
                + "(A.CBDATG * 1000000) + A.CBHORA = \n"
                + "(\n"
                + "SELECT \n"
                + "MAX((B.CBDATG * 1000000) + B.CBHORA) \n"
                + "FROM \n"
                + "PROFG" + LibVersion + ".FGCMB B\n"
                + "WHERE\n"
                + "B.CBMOED = ? AND \n"
                + "B.CBMOEB = ? AND  \n"
                + "B.CBTCOT = 'F' AND  \n"
                + "B.CBDATC <= ? \n"
                + ") with UR";
    }

    public String getCorrespondentByCurrency() {
        return "select "
                + "case when CICDIV is null then NOCDIV else CICDIV end as NOCDIV, "
                + "docban as NOCBAN, "
                + "NOSPAI, "
                + "DOSWIF, "
                + "NOIBAN, "
                + "cicpos as NOCVOS, "
                + "NOCNOS "
                + "from sispaga.FGDOMIC "
                + "left join sispaga.MMCNTI on CICBAN = DOCBAN and CISPAI = DOSPAI and CICBAL = DOCBAL and CICDOM = DOCDOM and DOKSWF = 'S' "
                + "left join sispaga.fgnost on DOCBAN = NOCBAN AND DOSPAI = NOSPAI AND DOCBAL = NOCBAL AND DOCDOM = NOCDOM and nositu = 'V' "
                + "left join sispaga.correspondentes on BIC = left(DOSWIF,8) "
                + "where (CICDIV = ? or NOCDIV = ?) and dositu = 'V' and NOSPAI is not null order by hierarquia with UR";
    }

    public String getPreferedCorrespondent() {
        return "select $WCBP,$WCPP from PROMOF" + LibVersion + ".$PMOEDA where $WSITU= 'V' and $WMOED = ? with UR";
    }

    public String getCurrencyCode() {
        return "SELECT $WNMOE FROM PROMOF" + LibVersion + ".$PMOEDA where $WSITU = 'V' and $WMOED = ? with UR";
    }

    public String getSwiftMsg() {
        return "SELECT SSMSG,SSEMSG FROM sispaga.FWSSWF WHERE SSCTOP = ? and SSNOPR = ? with UR";
    }

    public String getOperationAndAccountFromDescription() {
        return "select MVNOPR, GBMVCONTA, mvbal from GBANCA" + LibVersion + ".gbmvdo where mvdmov like ? with UR";
    }

    public String getAccountFromOperationNumber() {
        return "select MVNOPR, GBMVCONTA, mvbal, mvmoed from GBANCA" + LibVersion + ".gbmvdo where MVNOPR = ? with UR";
    }

    public String getAccountFromOperationNumberComissions() {
        return "select MVNOPR, GBMVCONTA, mvbal, mvmoed, mvcope, mvdmov from GBANCA" + LibVersion + ".gbmvdo where MVNOPR = ? and mvvlr < 0 with UR";
    }

    public String getSWIFTFields() {
        return "select * from sispaga.FWMSWF where MSCSW = ? order by mssord with UR";
    }

    public String getDataBanka() {
        return "SELECT left( PAVALV , 8) FROM gbanca" + LibVersion + ".gbparma WHERE PAPARM = 1 with UR";
    }

    public String getBankaStatus() {
        return "SELECT RIGHT(left(PAVALV,10),2) FROM gbanca" + LibVersion + ".gbparma WHERE PAPARM = 1 with UR";
    }

    public String getAuthorizationCode() {
        return "select $PPUCDGA from PROMOF" + LibVersion + ".$PPUSER where $PPUAPL = 'BM' and $PPUSITU = 'V' and $PPUUSER = ? with UR";
    }

    public String getApiExtraFields() {
        return " select GBECCGELEM, GBETDTPELE, GBECMANOPC " +
                " from gbanca" + LibVersion + ".gbctent, gbanca" + LibVersion + ".gbtelec " +
                " where GBETCGELEM = GBECCGELEM AND gbectpen = ? AND GBECSITU = 'V' AND GBETSITU = 'V' AND GBETVENTP = ? AND gbecseqord > 499 " +
                " order by gbecseqord with UR ";
    }

    public String getValidDayNotEUR() {
        return "select sum(total) from (\n"
                + "select \n"
                + "	count(*) as total\n"
                + "from \n"
                + "	PROMOF" + LibVersion + ".$PMOEDA moeda, \n"
                + "	PROMOF" + LibVersion + ".$PPAIS pais,\n"
                + "	PROFG" + LibVersion + ".FGFERIA feriado\n"
                + "where\n"
                + "	moeda.$WNMOE = pais.$ANPAI and \n"
                + "	pais.$ASITU = 'V' and \n"
                + "	moeda.$WSITU = 'V' and \n"
                + "	$WMOED = ? and\n"
                + "	FECPAI = $ACPAI and\n"
                + "	FEDATA = ?\n"
                + "union all	\n"
                + "select \n"
                + "case when DAUTIL = 'N' then 1 else 0 end as total\n"
                + "from PROFG" + LibVersion + ".FGDATA where dadat8 = ?) as a with UR";
    }

    public String getValidDayEUR() {
        return "select sum(total) from (\n"
                + "select \n"
                + "	count(*) as total\n"
                + "from \n"
                + "	PROFG" + LibVersion + ".FGFERIA feriado\n"
                + "where\n"
                + "	FECPAI = 'XE' and\n"
                + "	FEDATA = ?\n"
                + "union all	\n"
                + "select \n"
                + "case when DAUTIL = 'N' then 1 else 0 end as total\n"
                + "from PROFG" + LibVersion + ".FGDATA where dadat8 = ?) as a with UR";
    }

    public String getOperationDone() {
        return "select mvnopr from gbanca" + LibVersion + ".gbmvdo where mvdmov = ? and MVEANL = '' with UR";
    }

    public String getOperationDoneToday() {
        return "select mvnopr from gbanca" + LibVersion + ".gbmvdo where (mvdmov = ? or mvobs = ? ) and mvdatl = ? and MVEANL = '' with UR";
    }

    public String isClientBank() {
        return "select cltcli "
                + " from gbanca" + LibVersion + ".gbcli where clncli = ? with UR";
    }

    public String getOperationsCorrespondent() {
        return "select FGEXNOPR from profg" + LibVersion + ".fgextmm where FGEXCAPL  ='EX' and fgexintf = 'ADD_CORR  ' and fgexctop = ? and FGEXDV >= ? with UR"; // AQUI

//          return "select FGEXNOPR from profg0008.fgextmm where FGEXCAPL  ='EX' and fgexintf = 'ADD_CORR  ' and fgexctop = 'OPC' and FGEXDV >= 20180504 and "
//                  + "FGEXNOPR not in (select FGEXNOPR from profg0008.fgextmm where FGEXCAPL  ='EX' and fgexintf = 'DEL_CORR  ' and fgexctop = 'OPC' and FGEXDV >= '20180504' )"
//                  + " with UR";
    }

    public String getOperationsCO() {
        return "select\n"
                + "distinct\n"
                + "mrccb,\n"
                + "mrvlr,\n"
                + "mrmoed,\n"
                + "mrdatl,\n"
                + "mrnopr, \n"
                + "MRDMOV, \n"
                + "Mvdatv \n"
                + "from\n"
                + "gbanca" + LibVersion + ".gbmvco, \n"
                + "gbanca" + LibVersion + ".gbmvdo\n"
                + "where \n"
                + "mrnopr = mvnopr and\n"
                + "mrdatl >= ?  and\n"
                + "mrobs like ? and MRDMOV not like 'OPA%' and mreanl = '' and\n"
                + "mrcope in (214,215,216,217,218,220,221) with UR";
    }

    public String getOperationsCOXXX() {
        return "select\n"
                + "distinct\n"
                + "mrccb,\n"
                + "mrvlr,\n"
                + "mrmoed,\n"
                + "mrdatL,\n"
                + "mrnopr, \n"
                + "MRDMOV \n"
                + "from\n"
                + "gbanca" + LibVersion + ".gbmvco \n"
                //                + "gbanca" + LibVersion + ".gbmvdo\n"
                + "where \n"
                //                + "mrnopr = mvnopr and\n"
                + "mrnopr = 1339690  \n";
//                + "mrdatl >= ?  and\n"
//                + "mrobs like ? and mreanl = '' and\n"
//                + "mrcope in (214,215,216,217,218,220,221) with UR";
    }

    public String getOperationsCOOthers() {
        return "select\n"
                + "mrccb,\n"
                + "mrvlr,\n"
                + "mrmoed,\n"
                + "mrdtsis,\n"
                + "mrnopr, \n"
                + "MRDMOV \n"
                + "from\n"
                + "gbanca" + LibVersion + ".gbmvco\n"
                + "where \n"
                + "mrdatl >= ?  and\n"
                + "MRUSER = ? and mreanl = '' and\n"
                + "mrcope = ? with UR";
    }

    public String getOperationsCOOperationNumber() {
        return "select\n"
                + "mrccb,\n"
                + "mrvlr,\n"
                + "mrmoed,\n"
                + "mrdtsis,\n"
                + "mrnopr, \n"
                + "MRDMOV \n"
                + "from\n"
                + "gbanca" + LibVersion + ".gbmvco\n"
                + "where \n"
                + "mrnopr = ?  and\n"
                + "mrcope in (214,215,216,217,218,220,221) with UR";
    }

    public String getOperationFGEXTMM() {
        return "select fgexctop,FGEXNCB, FGEXNSP, FGEXNOPR, FGEXCDIV, FGEXDV, fgextmov \n"
                + "from profg" + LibVersion + ".fgextmm \n"
                + "where FGEXCAPL  ='EX' and fgexnopr = ?";
    }

    public String getClientByName() {
        return "select \n"
                + "ltncli as cliente,\n"
                + "ttnomt as nome\n"
                + "from \n"
                + "gbanca" + LibVersion + ".gbtit, \n"
                + "gbanca" + LibVersion + ".gbltit\n"
                + "where \n"
                + "ttnomt like ? and "
                + "ltntit = ttnumt";
    }

    public String getTableResults(String table) {
        return "select * from sispaga." + table;
    }

    public String getBicResults(String bic, String divisa) {
        return "select BIC, SP, REGRAS from sispaga.CORRESPONDENTES where BIC = ? and DIVISA = ?";
    }

    public Context(String input) {
        this.LibVersion = input;
    }

    public void setVersion(String input) {
        this.LibVersion = input;
    }

    public String getCorrespondente() {
        return "select bic from sispaga.correspondentes where divisa = ? order by hierarquia";
    }

    public String getIsVostro() {
        return "select * from gbanca" + LibVersion + ".gbcco where CSCCB = ? and CSMOED = ?";
    }

    public String getCambiosDivisas() {
        return "SELECT * FROM PROFG" + LibVersion + ".FGCMB3 where CBDATC = ? AND CBMOED = ? AND CBMOEB = ? AND CBTCOT = ?";
    }

    public String getTaxas() {
        return "SELECT * FROM GBANCA" + LibVersion + ".GBTXREF where TVTTXR = ? and TVDTCR = ? ";
    }

    public String getEntityByName() {
        return "SELECT * FROM GBANCA" + LibVersion + ".GBTIT where TTNOMT = ? ";
    }

    public String getEntityByDocTypeAndNumber() {
        return "SELECT * FROM GBANCA" + LibVersion + ".NCDOCENT where NCDCTPDOC = ? AND NCDCNDOC = ? ";
    }

    public String getEntBloqAML() {
        return "SELECT * FROM GBANCA" + LibVersion + ".GBBLOQ WHERE BQNUMT = ? AND BQSITU = 'V' AND BQBLOQ = 'A'";
    }

    public String getDpDetails() {
        return "SELECT * FROM GBANCA" + LibVersion + ".GBCDP WHERE GBDPCONTA = ?";
    }

    public String getDoEndDPMovement() {
        return "SELECT mvvlr FROM GBANCA" + LibVersion + ".GBMVDO INNER JOIN GBANCA" + LibVersion + ".GBMVDP ON MVNOPR = mpnopr WHERE GBMPCONTA = ? AND mpcope = ? ";
    }

    public String getDpEndDPMovement() {
        return "SELECT mpvlr FROM GBANCA" + LibVersion + ".GBMVDP, GBANCA" + LibVersion + ".GBCDP WHERE GBMPCONTA = GBDPCONTA AND GBMPCONTA = ? ";
    }

    public String getDpMovement() {
        return "SELECT MPVLR, MPDATL FROM GBANCA" + LibVersion + ".GBMVDP WHERE GBMPCONTA = ? AND mpcope = ?";
    }

    public String getNrClienteByEntityNr() {
        return "SELECT LTNCLI FROM GBANCA" + LibVersion + ".GBLTIT WHERE LTNTIT = ?";
    }

    public String getDoByClientNr() {
        return "SELECT GBDOCONTA FROM GBANCA" + LibVersion + ".GBCDO WHERE DONCLI = ?";
    }

    public String getCambiosDivisasLatest() {
        return "SELECT * FROM PROFG" + LibVersion + ".FGCMB3 where CBMOED = ? AND CBMOEB = ? AND CBTCOT = ? order by CBDATC desc FETCH FIRST 1 ROW ONLY";
    }

    public String getTaxasLatest() {
        return "SELECT * FROM GBANCA" + LibVersion + ".GBTXREF where TVTTXR = ? order by TVDTCR desc FETCH FIRST 1 ROW ONLY";
    }

    /**
     * Get DP account number by its:
     * <li>Client_number,</li>
     * <li>DO_account_number</li>
     * <li>description of movement</li>
     * @return string with query
     */
    public String getDpAccountNumber() {
        return "SELECT GBMPCONTA FROM GBANCA" + LibVersion + ".GBCDP, GBANCA" + LibVersion + ".GBMVDP WHERE GBMPCONTA = GBDPCONTA AND DPNCLI = ? AND GBDPCNTJ = ? AND MPDMOV = ? ";
    }

    public String getSepaNumber() {
        return "SELECT EINTOP, EIVLRT FROM GBANCA0008.TETEIS WHERE EITTEI = ? AND TEEICNTO = ? AND EICNTD = ? AND EIOBS = ? ";
    }



}