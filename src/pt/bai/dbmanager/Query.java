/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.bai.dbmanager;

/**
 *
 * @author loliveira
 */
public enum Query {
    getOperationDescription,
    getNOSTRO,
    getAllNOSTRO,
    getVOSTRO,
    getVOSTROOffline,
    getVOSTROCurrency,
    getVOSTROCurrencyOffline,
    getAccountNIB,
    getLocks,
    getTaxExemption,
    getClientInfo,
    getClientTotalInfo,
    getDOAccount,
    getClientAddresses,
    getClientFromAccount,
    getClientFromNIB,
    getBICfromNOSTRO,
    getBICfromVOSTRO,
    getNameFromCCB,
    getFixing,
    getCorrespondentByCurrency,
    getPreferedCorrespondent,
    getCurrencyCode,
    getCaractegorizationSWIFT,
    getCaractegorizationSWIFTOffline,
    getCaracterizationBankCountry,
    getSwiftMsg,
    getSWIFTFields,
    getOperationAndAccountFromDescription,
    getAccountFromOperationNumber,
    getDataBanka,
    getAuthorizationCode,
    getValidDayNotEUR,
    getValidDayEUR,
    getBANPAIFromNOSTRO,
    isClientBank,
    getOperationDone,
    getOperationDoneToday,
    getOperationsCorrespondent,
    getOperationsCO,
    getAccountFromOperationNumberComissions,
    getOperationFGEXTMM
}